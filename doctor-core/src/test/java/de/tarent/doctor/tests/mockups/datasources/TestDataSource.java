/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 12.10.2005
 */

package de.tarent.doctor.tests.mockups.datasources;

import java.util.List;
import java.util.Map;

import de.tarent.doctor.core.datasources.DoctorDataSource;

public class TestDataSource implements DoctorDataSource
{
    private Map parameters = null;
    
    public Object getData(Object key)
    {
        return "Test Data provided by TestDataSource with parameters " + parameters;
    }

    public void setData(Object key, Object value)
    {
        // NOP
    }

    public void setParameters(Map parameters)
    {
        this.parameters = parameters;
    }

    public boolean initialize()
    {
        return true;
    }

    public boolean dispose()
    {
        return true;
    }

    public List getAllData()
    {
        return null;
    }

}
