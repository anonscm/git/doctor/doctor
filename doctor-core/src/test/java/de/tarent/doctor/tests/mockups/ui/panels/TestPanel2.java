package de.tarent.doctor.tests.mockups.ui.panels;


import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import de.tarent.doctor.core.ui.PanelEvent;
import de.tarent.doctor.core.ui.WizardPanelControl;
import de.tarent.doctor.core.ui.WizardPanelUI;

public class TestPanel2 extends JPanel implements WizardPanelUI
{
    /** serialVersionUID */
	private static final long serialVersionUID = -1209793026522142375L;

	private WizardPanelControl controller = null;
    
    private JLabel strasseLabel = null;
    private JTextField strasseField = null;
    private JTextField ortField = null;

    private JLabel jLabel = null;
    
    public TestPanel2()
    {
    	this(null);
    }

    /**
     * This is the default constructor
     */
    public TestPanel2(WizardPanelControl controller)
    {
        super();
        this.controller = controller;
        initialize();
    }

    /**
     * This method initializes the panel
     * 
     */
    private void initialize()
    {
        // Construct GUI   
        this.setLayout(null);
        this.add(getOrtField(), null);
        jLabel = new JLabel();
        jLabel.setBounds(new java.awt.Rectangle(60,45,70,17));
        jLabel.setText("Ort:");
        this.add(jLabel, null);
        strasseLabel = new JLabel();
        strasseLabel.setText("Strasse:");
        strasseLabel.setBounds(new java.awt.Rectangle(60,15,58,15));
        this.add(strasseLabel, null);
        this.add(getStrasseField(), null);
        if(controller != null)
        	getStrasseField().setText((String)controller.getParameters().get("text"));
        this.setSize(468, 196);
    }

    /**
     * This method initializes nameField	
     * 	
     * @return javax.swing.JTextField	
     */
    private JTextField getStrasseField()
    {
        if (strasseField == null)
        {
            strasseField = new JTextField();
            strasseField.setPreferredSize(new java.awt.Dimension(200,19));
            strasseField.setBounds(new java.awt.Rectangle(135,15,200,19));
            strasseField.addKeyListener(new java.awt.event.KeyAdapter()
            {
                public void keyTyped(java.awt.event.KeyEvent e)
                {
                    sendEvent(PanelEvent.VALUE_CHANGED, "strasse", getStrasseField().getText());
                }
            });
        }
        return strasseField;
    }

    public Object getProperty(String propertyName)
    {
        if ("name".equals(propertyName))
            return getStrasseField().getText();
        else
            return null;
    }
    
    public boolean setProperty(String propertyName, Object name)
    {
        if ("name".equals(propertyName))
            getStrasseField().setText((String)name);
        
        return true;
    }
    
    private void sendEvent(int eventType, String propertyName, Object propertyValue)
    {
        controller.propertyChanged(new PanelEvent(this, eventType, propertyName, propertyValue));
    }
    
    /**
     * This method initializes jTextField	
     * 	
     * @return javax.swing.JTextField	
     */
    private JTextField getOrtField()
    {
        if (ortField == null)
        {
            ortField = new JTextField();
            ortField.setBounds(new java.awt.Rectangle(135,45,197,19));
            ortField.addKeyListener(new java.awt.event.KeyAdapter()
            {
                public void keyTyped(java.awt.event.KeyEvent e)
                {
                    sendEvent(PanelEvent.VALUE_CHANGED, "ort", getOrtField().getText());
                }
            });
        }
        return ortField;
    }
    
}  //  @jve:decl-index=0:visual-constraint="69,39"


