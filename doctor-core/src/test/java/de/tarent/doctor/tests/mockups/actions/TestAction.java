/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 14.10.2005
 */

package de.tarent.doctor.tests.mockups.actions;

import java.util.Map;

import de.tarent.commons.utils.Log;
import de.tarent.doctor.core.WizardData;
import de.tarent.doctor.core.actions.DoctorAction;

public class TestAction implements DoctorAction
{
    private Map parameters = null;

    public void invoke(WizardData wizardData)
    {
        Log.info(this.getClass(), "TestAction invoked: " + wizardData + " with parameters: " + parameters );
    }

    public void setParameters(Map parameters)
    {
        this.parameters = parameters;
    }
}
