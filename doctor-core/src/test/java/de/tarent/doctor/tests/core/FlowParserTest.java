/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 17.10.2005
 */

package de.tarent.doctor.tests.core;


import junit.framework.TestCase;

public class FlowParserTest extends TestCase
{

    /*
     * Test method for 'de.tarent.doctor.core.FlowParser.FlowParser(String, WizardData, String)'
     */
    public void testFlowParser()
    {
        //FlowParser flowParser = new FlowParser("src/tests/resources/wizard.xml", new WizardData(), "defaultflow");
        //assertNotNull(flowParser);
    }

    /*
     * Test method for 'de.tarent.doctor.core.FlowParser.getDataSources()'
     */
    public void testGetDataSources()
    {

    }

    /*
     * Test method for 'de.tarent.doctor.core.FlowParser.getEngineByName(String)'
     */
    public void testGetEngineByName()
    {

    }

    /*
     * Test method for 'de.tarent.doctor.core.FlowParser.getActionByName(String)'
     */
    public void testGetActionByName()
    {

    }

    /*
     * Test method for 'de.tarent.doctor.core.FlowParser.getDefaultEngine()'
     */
    public void testGetDefaultEngine()
    {

    }

    /*
     * Test method for 'de.tarent.doctor.core.FlowParser.next()'
     */
    public void testNext()
    {

    }

    /*
     * Test method for 'de.tarent.doctor.core.FlowParser.isLastEntry()'
     */
    public void testIsLastEntry()
    {

    }

    /*
     * Test method for 'de.tarent.doctor.core.FlowParser.isFirstEntry()'
     */
    public void testIsFirstEntry()
    {

    }

    /*
     * Test method for 'de.tarent.doctor.core.FlowParser.previous()'
     */
    public void testPrevious()
    {

    }

    /*
     * Test method for 'de.tarent.doctor.core.FlowParser.getHelpUrlByPanelName(String)'
     */
    public void testGetHelpUrlByPanelName()
    {

    }

}
