package de.tarent.doctor.tests.mockups.ui.panels;


import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.Element;

import de.tarent.doctor.core.ui.PanelEvent;
import de.tarent.doctor.core.ui.WizardPanelControl;
import de.tarent.doctor.core.ui.panels.HTMLPanel;

public class TestHTMLPanel extends HTMLPanel
{
    /** serialVersionUID */
	private static final long serialVersionUID = 8519360681497806312L;

	public TestHTMLPanel()
	{
		this((WizardPanelControl)null, (String)null);
	}
	
	public TestHTMLPanel(WizardPanelControl controller, String htmlFile)
    {
        super(controller, htmlFile);
    }

    protected void configureComponent(final String name, final Component component)
    {
        if (component instanceof JTextField)
            ((JTextField)component).addKeyListener(new KeyListener() {
                
                public void keyTyped(KeyEvent e)
                {
                    sendEvent(PanelEvent.VALUE_CHANGED, name, ((JTextField)component).getText());
                }

                public void keyPressed(KeyEvent e)
                {
                    // NOP
                }

                public void keyReleased(KeyEvent e)
                {
                    // NOP
                }                
            });
        else if (component instanceof JButton)
            ((JButton)component).addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e)
                {
                    sendEvent(PanelEvent.BT_PRESSED, name, null);
                }                
            });
        else if (component instanceof JSlider)
        {
            ((JSlider)component).setMajorTickSpacing(10);
            ((JSlider)component).setMinorTickSpacing(1);
            ((JSlider)component).setPaintTicks(true);
            ((JSlider)component).setPaintLabels(true);
            
            ((JSlider)component).addChangeListener(new ChangeListener() {
                public void stateChanged(ChangeEvent e)
                {
                    sendEvent(PanelEvent.VALUE_CHANGED, name, null);
                }                
            });
        }
        else
            throw new RuntimeException("(componentCreated Callback) Unsupported HTML widget type.");
    }

    public Object getProperty(String propertyName)
    {
        Object superResult = super.getProperty(propertyName);
        
        Component component = getComponentByName(propertyName);

        if (superResult==null)
            if (component instanceof JSlider)
                return new Integer(((JSlider)component).getValue());
            else
                throw new RuntimeException("(getProperty Callback) Unsupported HTML widget type.");

        return superResult;
    }
    
    public boolean setProperty(String propertyName, Object value)
    {
        Component component = getComponentByName(propertyName);
        
        if (component instanceof JSlider)
        {
            ((JSlider)component).setValue(((Integer)value).intValue());
            return true;
        }
        
        return false;
    }

	public void componentCreated(String arg0, Component arg1, Element arg2)
	{
		// TODO Auto-generated method stub
		
	}

}
