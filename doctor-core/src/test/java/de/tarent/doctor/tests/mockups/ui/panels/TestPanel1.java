package de.tarent.doctor.tests.mockups.ui.panels;


import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import de.tarent.doctor.core.ui.PanelEvent;
import de.tarent.doctor.core.ui.WizardPanelControl;
import de.tarent.doctor.core.ui.WizardPanelUI;

public class TestPanel1 extends JPanel implements WizardPanelUI
{
    /** serialVersionUID */
	private static final long serialVersionUID = -1763452649711402807L;

	private WizardPanelControl controller = null;
    
    private JLabel nameLabel = null;
    private JTextField vornameField = null;
    private JTextField nachnameField = null;
    private JLabel nachnameLabel = null;
    
    public TestPanel1()
    {
    	this(null);
    }

    /**
     * This is the default constructor
     */
    public TestPanel1(WizardPanelControl controller)
    {
        super();
        this.controller = controller;
        initialize();
    }

    /**
     * This method initializes the panel
     * 
     */
    private void initialize()
    {
        // Construct GUI   
        this.setLayout(null);
        this.add(getNachnameField(), null);
        nachnameLabel = new JLabel();
        nachnameLabel.setBounds(new java.awt.Rectangle(60,45,70,17));
        nachnameLabel.setText("Nachname:");
        this.add(nachnameLabel, null);
        nameLabel = new JLabel();
        nameLabel.setText("Vorname:");
        nameLabel.setBounds(new java.awt.Rectangle(60,15,58,15));
        this.add(nameLabel, null);
        this.add(getVornameField(), null);
        if(controller != null)
        	getVornameField().setText((String)controller.getParameters().get("text"));
        this.setSize(468, 196);
    }

    /**
     * This method initializes nameField	
     * 	
     * @return javax.swing.JTextField	
     */
    private JTextField getVornameField()
    {
        if (vornameField == null)
        {
            vornameField = new JTextField();
            vornameField.setPreferredSize(new java.awt.Dimension(200,19));
            vornameField.setBounds(new java.awt.Rectangle(135,15,200,19));
            vornameField.addKeyListener(new java.awt.event.KeyAdapter()
            {
                public void keyTyped(java.awt.event.KeyEvent e)
                {
                    sendEvent(PanelEvent.VALUE_CHANGED, "vorname", getVornameField().getText());
                }
            });
        }
        return vornameField;
    }

    public Object getProperty(String propertyName)
    {
        if ("name".equals(propertyName))
            return getVornameField().getText();
        else
            return null;
    }
    
    public boolean setProperty(String propertyName, Object name)
    {
        if ("name".equals(propertyName))
            getVornameField().setText((String)name);
        
        return true;
    }
    
    private void sendEvent(int eventType, String propertyName, Object propertyValue)
    {
        controller.propertyChanged(new PanelEvent(this, eventType, propertyName, propertyValue));
    }
    
    /**
     * This method initializes jTextField	
     * 	
     * @return javax.swing.JTextField	
     */
    private JTextField getNachnameField()
    {
        if (nachnameField == null)
        {
            nachnameField = new JTextField();
            nachnameField.setBounds(new java.awt.Rectangle(135,45,197,19));
            nachnameField.addKeyListener(new java.awt.event.KeyAdapter()
            {
                public void keyTyped(java.awt.event.KeyEvent e)
                {
                    sendEvent(PanelEvent.VALUE_CHANGED, "nachname", getVornameField().getText());
                }
            });
        }
        return nachnameField;
    }
    
}  //  @jve:decl-index=0:visual-constraint="69,39"


