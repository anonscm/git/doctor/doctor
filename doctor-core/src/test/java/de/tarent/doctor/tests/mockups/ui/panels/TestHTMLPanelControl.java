package de.tarent.doctor.tests.mockups.ui.panels;


import java.util.Map;

import de.tarent.commons.utils.Log;
import de.tarent.doctor.core.WizardData;
import de.tarent.doctor.core.ui.PanelEvent;
import de.tarent.doctor.core.ui.WizardFrame;
import de.tarent.doctor.core.ui.WizardPanelControl;
import de.tarent.doctor.core.ui.panels.HTMLPanel;

public class TestHTMLPanelControl extends WizardPanelControl
{    
    private HTMLPanel htmlPanel = null;
    
    protected void initialize(WizardFrame parent, WizardData wizardData, Map parameters)
    {
        this.setParent(parent);
        this.setWizardData(wizardData);
        this.setParameters(parameters);
        this.setValidated(false);
        htmlPanel = new TestHTMLPanel(this, (String)parameters.get("htmlurl"));
        setUI(htmlPanel);
    }

    public void propertyChanged(PanelEvent e)
    {
        Log.info(this.getClass(), "PanelEvent: " + e.getEventType() + " - " + e.getPropertyName() + " = " + e.getPropertyValue());
        // getWizardData().put(e.getPropertyName(), e.getPropertyValue());
    }

}
