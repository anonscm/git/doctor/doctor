package de.tarent.doctor.tests.mockups.ui.panels;


import java.util.Map;

import de.tarent.doctor.core.WizardData;
import de.tarent.doctor.core.ui.PanelEvent;
import de.tarent.doctor.core.ui.WizardFrame;
import de.tarent.doctor.core.ui.WizardPanelControl;

public class TestControl2 extends WizardPanelControl
{    
    protected void initialize(WizardFrame parent, WizardData wizardData, Map parameters)
    {
        this.setParent(parent);
        this.setWizardData(wizardData);
        this.setParameters(parameters);
        this.setValidated(false);
        setUI(new TestPanel2(this));
    }

    public void propertyChanged(PanelEvent e)
    {
        getWizardData().put(e.getPropertyName(), e.getPropertyValue());
        
        if (getWizardData().containsKey("strasse") && getWizardData().containsKey("ort"))
            this.setValidated(true);
    }
}
