/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 12.10.2005
 */

package de.tarent.doctor.tests.mockups.engines;

import java.util.Map;

import de.tarent.doctor.core.engines.DoctorGDGEngine;

public class TestEngine extends DoctorGDGEngine
{
    private Map parameters = null;
    
    public void process()
    {
        System.out.println("Test Engine starts with wizard data: " + wizardData + " and parameters: " + parameters );
    }
}
