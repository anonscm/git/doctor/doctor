/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 12.10.2005
 */

package de.tarent.doctor.core.engines;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.evolvis.liboffice.OfficeAutomat;
import org.evolvis.liboffice.controller.OfficeController;
import org.evolvis.liboffice.controller.OfficeControllerToolKit;
import org.evolvis.liboffice.controller.ReplacementToolKit;
import org.evolvis.liboffice.factory.MainOfficeFactory;
import org.evolvis.liboffice.ui.swing.conf.FirstRunWizardSwing;

/**
 * Defines the interface for all Doctor engines performing actions on office-documents throug the General Document Generator
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public abstract class DoctorGDGEngine extends DoctorEngine
{
	private final static Logger logger = Logger.getLogger(DoctorGDGEngine.class.getName()); 
	
    public DoctorGDGEngine()
    {
        initializeOfficeConnection();
    }
    
    /**
     * Determines the currently running office software and initiates
     * a connection to it. Then gets all needed factories.
     */
    protected void initializeOfficeConnection()
    {
    	if(!OfficeAutomat.getOfficeConfigurator().loadConfiguration(null))
		{
			OfficeAutomat.getOfficeConfigurator().setup(new FirstRunWizardSwing(null));
			OfficeAutomat.getOfficeConfigurator().saveConfiguration(null);
		}
		
		if(OfficeAutomat.getOfficeConfigurator().getPreferredOffice() == null)
		{
			if(logger.isLoggable(Level.WARNING)) logger.warning("You did not properly configure an office-environment! exiting...");
			//System.exit(1)
		}
		
		OfficeAutomat.getOfficeConfigurator().getPreferredOffice().setupEnvironmentVariables();
    }
    
    /**
     * Returns the ControllerFactory.
     * 
     * @return ControllerFactory.
     */
    
    /* Actually not needed
    protected ControllerFactory getControllerFactory()
    {
        return null;
    }
    */

    /**
     * Returns the OfficeControllerToolkit.
     * 
     * @return OfficeControllerToolKit
     */
    protected OfficeControllerToolKit getOfficeControllerToolKit()
    {
        return OfficeAutomat.getOfficeControllerToolKit();
    }
    
    /**
     * Returns the ReplacementToolkit.
     * 
     * @return OfficeControllerToolKit
     */
    protected ReplacementToolKit getReplacementToolKit()
    {
        return OfficeAutomat.getReplacementToolKit();
    }

    /**
     * Returns the MainOfficeFactory.
     * 
     * @return MainOfficeFactory
     */
    protected MainOfficeFactory getMainFactory()
    {
        return OfficeAutomat.getMainOfficeFactory();
    }

    /**
     * Returns the OfficeController.
     * 
     * @return OfficeController
     */
    protected OfficeController getOfficeController()
    {
        return OfficeAutomat.getOfficeController();
    }
    
    // Abstract methods follow
    
    /**
     * Processes the given wizard data to produce the documents using
     * the GDG API. This method should also clean up resources when
     * finishing work.
     */
    public abstract void process() throws InterruptedException;
}
