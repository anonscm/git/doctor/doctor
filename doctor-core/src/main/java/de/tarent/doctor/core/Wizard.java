/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 07.10.2005
 */

package de.tarent.doctor.core;

import java.util.Iterator;

import de.tarent.commons.utils.Log;
import de.tarent.doctor.core.datasources.DoctorDataSource;
import de.tarent.doctor.core.engines.DoctorEngine;
import de.tarent.doctor.core.ui.Messages;
import de.tarent.doctor.core.ui.ProgressDialog;
import de.tarent.doctor.core.ui.WizardFrame;
import de.tarent.doctor.core.utils.DoctorQualityFeedbackAgent;

/**
 * This is the main class of the Doctor. It initializes all needed classes, opens
 * the main window and delegates control to the Swing thread(s). After finishing
 * the UI, control is returning to this class, executing the selected engine with
 * the generated wizard data.
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public class Wizard
{
    private FlowParser parser = null;
    private WizardData wizardData = null;
    private WizardFrame wizardFrame = null;
    private ProgressDialog progressDialog = null;
    private DoctorEngine engine = null;
    private boolean managedMode = false;
    
    /**
     * Starts a new Doctor UI by opening the main window and delegating
     * control to Swing.
     * 
     * @param configFile Path to the configuration file.
     * @param flowName The name of the flow that should be executed.
     * @param dataFile Optional path to a file with a pre-defined set of key-value pairs for the wizardData instance. If this is not null the UI will be skipped and the engine will run directly after startup using the data in the specified file.
     */
    public Wizard(String configFile, String flowName, String dataFile, boolean pManagedMode)
    {
        // Setting font rendering to antialiased
        System.setProperty("swing.aatext", "true");
        
        managedMode = pManagedMode;
        
        try
        {
            // Create new wizardData and parse the optional data file
            wizardData = new WizardData();
            if (dataFile!=null)
                wizardData.loadFromFile(dataFile);
            
        
            // Parse the XML
            parser = new FlowParser(configFile, wizardData, flowName);

            // Display frame if data is not read from file
            wizardFrame = new WizardFrame(this, parser, wizardData);
            wizardFrame.setLocationRelativeTo(null);
            if (dataFile==null)
            {
                wizardFrame.setVisible(true);
            }
            else
                // Skip to engine execution
                this.UIFinished();
        }
        catch (RuntimeException e)
        {
            e.printStackTrace();
            new DoctorQualityFeedbackAgent(e.getMessage(), e);
            if(!managedMode) System.exit(-1);
        }
    }

    /**
     * This method is called by the event system when the Doctor
     * UI has finished. It determines the engine that should be
     * used by looking for a field "engine" in the wizard data. If 
     * this field is missing or contains null, the default engine
     * is used. The default engine is marked with 'default="true"' 
     * in the configuration.
     * <p>
     * After getting the engine, the method engine.process() is called
     * and the wizard data is provided to the engine.
     */
    public void UIFinished()
    {
        // Hide Wizard Frame
        wizardFrame.setVisible(false);
        wizardFrame.dispose();
        wizardFrame = null;

        // trigger B2B process
        String selectedEngine = (String)wizardData.get("engine");
        if (selectedEngine!=null)
        {
            engine = parser.getEngineByName(selectedEngine);
            if(engine == null)
            	throw new RuntimeException("Selected engine \""+selectedEngine+"\"could not be loaded");
        }
        else
            engine = parser.getDefaultEngine();
        
        if(engine == null)
        	throw new RuntimeException("Default engine could not be loaded");

        engine.setWizardData(wizardData);
        engine.setWizardData(this);

        // Display progress window
        progressDialog = new ProgressDialog(this, Messages.getString("WizardFrame.DialogTitleProgress")); //$NON-NLS-1$
        engine.setProgressReceiver(progressDialog);

        // Start processing
        engine.start();
    }

    /**
     * This method is called from the ProgressDialog when the cancel
     * button is pressed. It should halt the engine and terminate 
     * operation of the wizard.
     */
    public void cancelEngine()
    {
        engine.interrupt();
        doctorFinished();
    }
    
    /**
     * This is called from the progress dialog when is detects that
     * the engine is done. This method should clean up all wizard
     * resources and gracefully end the program execution.
     */
    public void doctorFinished()
    {
        // remove progressDialog
        progressDialog.setVisible(false);
        progressDialog.dispose();   
        progressDialog = null;

        // Clean up things
        Iterator iter = FlowParser.getDataSources().values().iterator();
        while (iter.hasNext())
            ((DoctorDataSource)iter.next()).dispose();

        // End execution
        Log.info(this.getClass(), "Doctor Wizard ends...");
        if(!managedMode) System.exit(0);
    }
    
    //TODO: ExtDoctor: Textbausteinverwaltung
    //TODO: ExtDoctor: Context-based actions on words/paragraphs: Definitions (e.g. Wikipedia), Translation (LEO), Translation of a paragraph , Google&Co Search
}
