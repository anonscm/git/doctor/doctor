/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 02.12.2005
 */

package de.tarent.doctor.core;

/**
 * This class represents an external tool read from the configuration.
 * It stores all Information of the current tool.
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public abstract class ExternalTool
{
    private String name = null;
    
    /**
     * Gets the name of this tool.
     * 
     * @return Name of tool.
     */
    public String getName()
    {
        return name;
    }

    /**
     * Sets the name of this tool.
     * 
     * @param name Name of tool.
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Runs the tool.
     */
    public abstract void execute(WizardData wizardData);
}
