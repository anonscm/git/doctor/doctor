/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 06.10.2005
 */

package de.tarent.doctor.core.ui;

/**
 * This interface must be implemented by all ui components of panels. To be
 * compatible with the Eclipse VE, all panel classes have to be directly derived
 * of JPanel. Therefore it is not possible to generalize implementations of 
 * registerPanelListener(), removePanelListener() and the underlying signal logic
 * to a father class. 
 * 
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public interface WizardPanelUI
{
    /**
     * This returns the property of an ui element referenced thru name. This
     * unifies the access to all payload data and state information in the gui 
     * to one common interface, regardless how the ui implements the specific 
     * ui element displaying the data. All conversion between ui storage formats or
     * methods and the format expected by the controller is done in this method.
     * 
     * @return Current value of the component.
     */
    public Object getProperty(String propertyName);

    /**
     * Sets a property of an abstract component. Like getValue(), this
     * method encapsulates the specific ui representation of an abstract
     * data.
     * 
     * @param value Value to be set.
     * @return True if setting of value was successful, false otherwise.
     */
    public boolean setProperty(String propertyName, Object value);
}
