/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 31.10.2005
 */

package de.tarent.doctor.core.engines;

public interface GroovyEngine
{
    public void run(DoctorEngine engine);
}
