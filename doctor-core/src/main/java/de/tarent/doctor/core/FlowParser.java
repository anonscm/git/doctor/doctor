/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 11.10.2005
 */

package de.tarent.doctor.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import de.tarent.commons.utils.Log;
import de.tarent.doctor.core.actions.DoctorAction;
import de.tarent.doctor.core.comm.DoctorCommunicator;
import de.tarent.doctor.core.datasources.DoctorDataSource;
import de.tarent.doctor.core.engines.DoctorEngine;
import de.tarent.doctor.core.ui.Messages;
import de.tarent.doctor.core.ui.StepEntry;

/**
 * This class parses the main configuration file. This file contains the definitions of
 * panels, engines, datasources, and the flow. The flow describes the course of a
 * wizard, which panel comes after which and so on.
 * <p>
 * The parser reads the config and parses all data except the flow into internal
 * data structures. The flow is parsed step-by-step by repeated calls to next().
 * <p>
 * To provide a system for going back from one panel to the previous, the parser
 * keeps a "history" of each step. If previous() is called, the last panel
 * is calculated based on the history entries. Keep in mind that the wizard data
 * that is to be modified by the wizard will not roll back when going to the
 * previous panel. Thus, the panels have to keep track of what is already 
 * in the wizard data map.
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public class FlowParser
{
    private Map nodeMap = null;
    private Map enginesMap = null;
    private Map actionsMap = null;
    private static Map dataSourcesMap = null;
    private static Map htmlWidgetsMap = null;
    private static Map externalToolsMap = null;
    private static Map communicatorsMap = null;
    private Node contextNode = null;
    private Document document = null;
    private WizardData properties = null;
    private List stepHistory = null;
    private boolean firstPanelDisplayed = true;
    private boolean lastPanelDisplayed = false;
    private DoctorEngine defaultEngine = null;
    private int wizardWidth = 0;
    private int wizardHeight = 0;

    /**
     * Initalizes the parser. It reads the given configuration file and
     * parses it into the internal structure while setting up the
     * step engine for reading the flow.
     * 
     * @param xmlFileName Filename of the configuration file.
     * @param pProperties The data that is to be modified by the various panels.
     * @param name The name of the flow that should be parsed.
     */
    public FlowParser(String xmlFileName, WizardData pProperties, String name)
    {
        stepHistory = new ArrayList();
        
        nodeMap = new HashMap();
        enginesMap = new HashMap();
        dataSourcesMap = new HashMap();
        htmlWidgetsMap = new HashMap();
        externalToolsMap = new HashMap();
        communicatorsMap = new HashMap();
        actionsMap = new HashMap();
        properties = pProperties;
        
        // Read the XML into a document
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try
        {
            builder = factory.newDocumentBuilder();
            document = builder.parse(xmlFileName);
        }
        catch (ParserConfigurationException e)
        {
            throw new RuntimeException(e);
        }
        catch (SAXException e)
        {
            throw new RuntimeException(e);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
        
        parseCoreData(document);
        parseHTMLWidgetDefinitions(document);
        parseExternalToolsDefinitions(document);
        parseCommunicatorsDefinitions(document);
        parsePanelDefinitions(document);
        parseActionDefinitions(document);
        parseEngineDefinitions(document);
        parseDataSourceDefinitions(document);
        parseFlow(document, name);
    }

    private void parseCommunicatorsDefinitions(Document document)
    {
    	NodeList rootNodes = document.getElementsByTagName("communicators");
    	
    	if(rootNodes == null || rootNodes.getLength() < 1) return;
    	
        NodeList commNodes = rootNodes.item(0).getChildNodes();
        for (int i=0; i<commNodes.getLength(); i++)
        {
            Node thisNode = commNodes.item(i);
            if (thisNode.getNodeName()!=null && thisNode.getNodeName().equals("communicator"))
            {
                // Communicator Node
                Element thisComm = (Element)thisNode;
                String commName = thisComm.getAttribute("name");
                String commClassName = thisComm.getAttribute("class");
                
                // Parse optional parameters
                Map parameters = parseParameters((Element)thisComm.getElementsByTagName("parameters").item(0));
                
                try
                {
                    DoctorCommunicator comm = (DoctorCommunicator)Class.forName(commClassName).newInstance();
                    comm.setParameters(parameters);
                    communicatorsMap.put(commName, comm);
                }
                catch (InstantiationException e)
                {
                    throw new RuntimeException("Error getting engine class: " + commClassName);
                }
                catch (IllegalAccessException e)
                {
                    throw new RuntimeException("Error getting engine class: " + commClassName);
                }
                catch (ClassNotFoundException e)
                {
                    throw new RuntimeException("Error getting engine class: " + commClassName);
                }
            }
        }
    }

    private void parseExternalToolsDefinitions(Document document)
    {
    	NodeList rootNodes = document.getElementsByTagName("externaltools");
    	
    	if(rootNodes == null || rootNodes.getLength() < 1) return;
    	
        NodeList toolsNodes = rootNodes.item(0).getChildNodes();
        for (int i=0; i<toolsNodes.getLength(); i++)
        {
            Node thisNode = toolsNodes.item(i);
            if (thisNode.getNodeName()!=null && thisNode.getNodeName().equals("tool"))
            {
                // Tool Node
                Element thisTool = (Element)thisNode;
                ExternalTool tool = null;
                
                String toolName = Messages.getString(thisTool.getAttribute("name"));

                if ("java".equals(thisTool.getAttribute("type").toLowerCase()))
                {
                    // Java-style tool
                    String toolClass = thisTool.getAttribute("class");

                    tool = new JavaTool(toolName, toolClass);
                }
                else if ("shell".equals(thisTool.getAttribute("type").toLowerCase()))
                {
                    // Shell-style tool
                    String toolCommand = thisTool.getAttribute("command");
                    String toolWorkingDir = thisTool.getAttribute("workingdir");

                    tool = new ShellTool(toolName, toolCommand, toolWorkingDir);
                }
                else
                    Log.error(this.getClass(), "Unknown tool type: " + thisTool.getAttribute("type"));
                
                externalToolsMap.put(toolName, tool);
            }
        }
    }
    
    private void parseHTMLWidgetDefinitions(Document document)
    {
    	NodeList rootNodes = document.getElementsByTagName("htmlwidgets");
    	
    	if(rootNodes == null || rootNodes.getLength() < 1) return;
    	
        NodeList widgetNodes = rootNodes.item(0).getChildNodes();
        for (int i=0; i<widgetNodes.getLength(); i++)
        {
            Node thisNode = widgetNodes.item(i);
            if (thisNode.getNodeName()!=null && thisNode.getNodeName().equals("widget"))
            {
                // HTMLWidget Node
                Element thisWidget = (Element)thisNode;
                String widgetType = thisWidget.getAttribute("type");
                String widgetClassName = thisWidget.getAttribute("class");
                
                // Parse optional parameters
                // Map parameters = parseParameters((Element)thisWidget.getElementsByTagName("parameters").item(0));
                
                try
                {
                    Class widget = Class.forName(widgetClassName);
                    // source.setParameters(parameters);
                    htmlWidgetsMap.put(widgetType, widget);
                }
                catch (ClassNotFoundException e)
                {
                    throw new RuntimeException("Error getting widget class: " + widgetClassName);
                }
            }
        }
    }


    /**
     * Parses some core data provided in the head of the
     * configuration, for example the width and height of
     * the window.
     * 
     * @param document Document to be parsed.
     */
    private void parseCoreData(Document document)
    {
        wizardWidth = Integer.parseInt(((Element)document.getDocumentElement()).getAttribute("width"));
        wizardHeight = Integer.parseInt(((Element)document.getDocumentElement()).getAttribute("height"));
    }

    /**
     * Returns the parsed datasources of the configuration.
     * 
     * @return datasources map: key it the name, value is an instance of the datasource class.
     */
    public static Map getDataSources()
    {
        return dataSourcesMap;
    }
    
    /**
     * Returns the parsed html widgets of the configuration.
     * 
     * @return Widgets map: key it the type, value is the class of the widget.
     */
    public static Map getHTMLWidgets()
    {
        return htmlWidgetsMap;
    }

    private void parseEngineDefinitions(Document document)
    {
    	NodeList rootNodes = document.getElementsByTagName("engines");
    	
    	if(rootNodes == null || rootNodes.getLength() < 1) return;
    	
        NodeList engineNodes = rootNodes.item(0).getChildNodes();
        for (int i=0; i<engineNodes.getLength(); i++)
        {
            Node thisNode = engineNodes.item(i);
            if (thisNode.getNodeName()!=null && thisNode.getNodeName().equals("engine"))
            {
                // Engine Node
                Element thisEngine = (Element)thisNode;
                String engineName = thisEngine.getAttribute("name");
                String engineClassName = thisEngine.getAttribute("class");
                
                // Parse optional parameters
                Map parameters = parseParameters((Element)thisEngine.getElementsByTagName("parameters").item(0));
                
                try
                {
                    DoctorEngine engine = (DoctorEngine)Class.forName(engineClassName).newInstance();
                    engine.setParameters(parameters);
                    enginesMap.put(engineName, engine);
                }
                catch (InstantiationException e)
                {
                    throw new RuntimeException("Error getting engine class: " + engineClassName, e);
                }
                catch (IllegalAccessException e)
                {
                    throw new RuntimeException("Error getting engine class: " + engineClassName, e);
                }
                catch (ClassNotFoundException e)
                {
                    throw new RuntimeException("Error getting engine class: " + engineClassName, e);
                }
            }
        }
    }

    private void parseActionDefinitions(Document document)
    {
    	NodeList rootNodes = document.getElementsByTagName("actions");
    	
    	if(rootNodes == null || rootNodes.getLength() < 1) return;
    	
        NodeList actionsNodes = rootNodes.item(0).getChildNodes();
        for (int i=0; i<actionsNodes.getLength(); i++)
        {
            Node thisNode = actionsNodes.item(i);
            if (thisNode.getNodeName()!=null && thisNode.getNodeName().equals("action"))
            {
                // Action Node
                Element thisAction = (Element)thisNode;
                String actionName = thisAction.getAttribute("name");
                String actionClassName = thisAction.getAttribute("class");

                // Parse optional parameters
                Map parameters = parseParameters((Element)thisAction.getElementsByTagName("parameters").item(0));
                
                try
                {
                    DoctorAction action = (DoctorAction)Class.forName(actionClassName).newInstance();
                    action.setParameters(parameters);
                    actionsMap.put(actionName, action);
                }
                catch (InstantiationException e)
                {
                    throw new RuntimeException("Error getting action class: " + actionClassName);
                }
                catch (IllegalAccessException e)
                {
                    throw new RuntimeException("Error getting action class: " + actionClassName);
                }
                catch (ClassNotFoundException e)
                {
                    throw new RuntimeException("Error getting action class: " + actionClassName);
                }
            }
        }
    }

    private void parseDataSourceDefinitions(Document document)
    {
    	NodeList rootNodes = document.getElementsByTagName("datasources");
    	
    	if(rootNodes == null || rootNodes.getLength() < 1) return;
    	
        NodeList dataSourceNodes = rootNodes.item(0).getChildNodes();
        for (int i=0; i<dataSourceNodes.getLength(); i++)
        {
            Node thisNode = dataSourceNodes.item(i);
            if (thisNode.getNodeName()!=null && thisNode.getNodeName().equals("datasource"))
            {
                // DataSource Node
                Element thisSource = (Element)thisNode;
                String sourceName = thisSource.getAttribute("name");
                String sourceClassName = thisSource.getAttribute("class");
                
                // Parse optional parameters
                Map parameters = parseParameters((Element)thisSource.getElementsByTagName("parameters").item(0));
                
                try
                {
                    DoctorDataSource source = (DoctorDataSource)Class.forName(sourceClassName).newInstance();
                    source.setParameters(parameters);
                    if (!source.initialize())
                        throw new RuntimeException("DataSource could not be initialized: " + sourceName);
                    dataSourcesMap.put(sourceName, source);
                }
                catch (InstantiationException e)
                {
                    throw new RuntimeException("Error getting datasource class: " + sourceClassName);
                }
                catch (IllegalAccessException e)
                {
                    throw new RuntimeException("Error getting datasource class: " + sourceClassName);
                }
                catch (ClassNotFoundException e)
                {
                    throw new RuntimeException("Error getting datasource class: " + sourceClassName);
                }
            }
        }
    }

    /**
     * Gets a configured engine by it's name that is associated to the engine
     * in the configuration.
     * 
     * @param engineName Symbolic name of the engine.
     * @return Instance of the engine.
     */
    public DoctorEngine getEngineByName(String engineName)
    {
        return (DoctorEngine)enginesMap.get(engineName);
    }
    
    /**
     * Gets a configured action by it's name that is associated to the action
     * in the configuration.
     * 
     * @param actionName Symbolic name of the action.
     * @return Instance of the action.
     */
    public DoctorAction getActionByName(String actionName)
    {
        return (DoctorAction)actionsMap.get(actionName);
    }

    /**
     * Gets the default engine defined in the configuration. The entry returned
     * is marked with the xml attribute 'default="true"'.
     * 
     * @return Default engine.
     */
    public DoctorEngine getDefaultEngine()
    {
        return defaultEngine;
    }
    
    private void parsePanelDefinitions(Document document)
    {
        // Read all panel definitions
    	NodeList rootNodes = document.getElementsByTagName("panels");
    	
    	if(rootNodes == null || rootNodes.getLength() < 1) return;
    	
        NodeList XmlPanelList = rootNodes.item(0).getChildNodes();
        for (int i=0; i<XmlPanelList.getLength(); i++)
            parsePanelDefinitionElements(nodeMap, XmlPanelList.item(i));
    }

    private void parsePanelDefinitionElements(Map nodeMap, Node panelNode)
    {
        if (panelNode.getNodeType()==Node.ELEMENT_NODE && panelNode.getNodeName().equals("panel"))
        {
            Element thisPanel = (Element)panelNode;
            
            String thisName = thisPanel.getAttribute("name");

            String thisHelpFileName = thisPanel.getAttribute("helpurl");
            
            String thisControllerClassName = thisPanel.getAttribute("controller");
            
            lastPanelDisplayed = thisPanel.getAttribute("last")!=null && thisPanel.getAttribute("last").equals("true");

            Map thisParameters = parseParameters((Element)((Element)panelNode).getElementsByTagName("parameters").item(0));
                
            PanelRegistryEntry thisEntry = new PanelRegistryEntry(thisName, thisControllerClassName, thisParameters, lastPanelDisplayed, thisHelpFileName);
            nodeMap.put(thisName, thisEntry);
        }
    }


    private Map parseParameters(Element parameterElement)
    {
        if (parameterElement==null)
            return null;
        
        NodeList thisParameterList = parameterElement.getChildNodes();
        Map thisParameters = new HashMap();
        for (int i=0; i<thisParameterList.getLength(); i++)
        {
            Node thisParameter = thisParameterList.item(i);
            if (thisParameter.getNodeType()==Node.ELEMENT_NODE && thisParameter.getNodeName().equals("parameter"))
            {
                String thisKey = ((Element)thisParameter).getAttribute("key");
                String thisValue = ((Element)thisParameter).getAttribute("value");
                thisParameters.put(thisKey, thisValue);
            }
        }
        return thisParameters;
    }

    private void parseFlow(Document document, String name)
    {
        // Read and parse the flow: use the topmost panel element only - don't use getElementsByTagname() here!
        NodeList XmlFlow = document.getDocumentElement().getChildNodes();
        
        boolean found = false;
        
        for (int i=0; i<XmlFlow.getLength(); i++)
        {
            Node thisNode = XmlFlow.item(i);
            if (thisNode.getNodeName().equals("flow") && ((Element)thisNode).getAttribute("name").equals(name))
            {
            	found = true;
                defaultEngine = getEngineByName(((Element)XmlFlow.item(i)).getAttribute("defaultengine"));
                
                if (contextNode!=null)
                    throw new RuntimeException("Flow already parsed.");
                contextNode=XmlFlow.item(i).getFirstChild();
            }
        }
        
        if(!found)
        	throw new RuntimeException("No flow with the specified name \""+name+"\" found.");
    }
                    
    private boolean checkProperty(String key, String value)
    {
        return properties.checkProperty(key, value);
    }

    /**
     * Returns the logical next panel to be displayed. It is calculated from the 
     * flow defined in the configuration and the current state of the
     * properties in the wizard data. 
     * 
     * @return Next panel to be displayed.
     */
    public PanelRegistryEntry next()
    {
        PanelRegistryEntry nextPanel = null;
        
        if (contextNode==null)
            throw new RuntimeException("Specified flow does not exist!");

        if (stepHistory.isEmpty())
            firstPanelDisplayed  = true;
        else
            firstPanelDisplayed = false;
        
        // Put this step into history - this is because the contextNode is modified in
        // the course of calculating the next panel - this results in going back
        // _two_ steps in the history when getting the previous panel!
        stepHistory.add(new StepEntry(contextNode));

        nextPanel = getNextPanel();
        
        if(nextPanel != null)
        	lastPanelDisplayed = nextPanel.isLastPanel();
        
        return nextPanel;
    }

    private PanelRegistryEntry getNextPanel()
    {        
        Element nextRelevantElement = getNextRelevantElement();
        
        if (nextRelevantElement==null)
            return null;
        
        if (nextRelevantElement.getNodeName().equals("show-panel"))
        {
            return (PanelRegistryEntry)nodeMap.get(nextRelevantElement.getAttribute("name"));
        }
        else if (nextRelevantElement.getNodeName().equals("on"))
        {
            String property = nextRelevantElement.getAttribute("property");
            String value = nextRelevantElement.getAttribute("value");
            
            // if on test fails, skip body (reposition contextNode)
            if (!checkProperty(property, value))
                contextNode = contextNode.getNextSibling();
            
            return getNextPanel();
        }
        else if (nextRelevantElement.getNodeName().equals("invoke-action"))
        {
            // invoke action by name
            getActionByName(nextRelevantElement.getAttribute("name")).invoke(properties);
            return getNextPanel();
        }
        else 
            return getNextPanel();
    }

    /**
     * Checks whether the current displayed panel is the last panel in
     * the flow. This is only true for 'stop panels' which are marked 
     * with the xml attribute 'last="true"' in the panel definition.
     * 
     * @return True if the current panel is the last panel in the flow, false otherwise.
     */
    public boolean isLastEntry()
    {
        return lastPanelDisplayed;
    }
    
    /**
     * Checks whether the current panel is the first panel in the flow. This
     * is true if the history of panels is empty and thus, the current panel
     * has no previous panel.
     * 
     * @return True if there is no previous panel in the flow, false otherwise.
     */
    public boolean isFirstEntry()
    {
        return firstPanelDisplayed;
    }

    /**
     * Returns the logical previous panel in the flow. It is calculated from
     * the history of panels already displayed and the state of the wizard data.
     * Because the wizard data is <b>not</b> rolled back, it is theoretically 
     * possible to reach a wrong panel. This must be considered when writing panels
     * and flows, especially flow fragments with on statements. 
     * 
     * @return The previous panel to be displayed.
     */
    public PanelRegistryEntry previous()
    {
        StepEntry previousStep = (StepEntry)stepHistory.get(stepHistory.size()-2);
        Node previousContextNode = previousStep.getContextNode();
        
        // remove the current displayed panel from history
        stepHistory.remove(stepHistory.size()-1);

        // remove the previous panel from history - this will be re-inserted by next()
        // note that the index has been altered by removing the current panel!
        stepHistory.remove(stepHistory.size()-1);
        
        contextNode = previousContextNode;

        return next();
    }

    private Element getNextRelevantElement()
    {
        if (getNextNode()==null)
            return null;
        String thisElementName = contextNode.getNodeName();
        if (thisElementName.equals("show-panel") || thisElementName.equals("on") || thisElementName.equals("flow") || thisElementName.equals("invoke-action"))
            return (Element)contextNode;
        else
        {
            return getNextRelevantElement();
        }
    }
        
    private Node getNextNode()
    {
        // First, check if childs are available - if yes: decend
        if (contextNode.hasChildNodes())
        {
            contextNode = contextNode.getFirstChild();
        }
        // if not, check siblings - if available: go to next sibling
        else if (contextNode.getNextSibling()!=null)
        {
            contextNode = contextNode.getNextSibling();
        }
        // No childs or siblings - search for next availabe sibling of a parent all the way up to the root
        else 
        {
            Node relevantParent = contextNode.getParentNode();
            // If we bump into the document node, end the run
            if (relevantParent.equals(document.getDocumentElement()))
                return null;
            while (relevantParent.getNextSibling()==null)
                relevantParent = relevantParent.getParentNode();
            contextNode = relevantParent.getNextSibling();
        }
        
        return contextNode;
    }

    /**
     * Returns the URL to the help HTML page of a panel referenced by name.
     * 
     * @param name Name of the panel.
     * @return filename of help HTML page as string.
     */
    public String getHelpUrlByPanelName(String name)
    {
        Iterator iter = nodeMap.values().iterator();

        while (iter.hasNext())
        {
            PanelRegistryEntry thisEntry = (PanelRegistryEntry)iter.next();
            if (thisEntry.getName().equals(name))
                return thisEntry.getHelpFileName();
        } 
        
        return null;
    }

    /**
     * Returns the width of the wizard window as configured in
     * the wizard.xml configuration.
     * 
     * @return Width of the window in pixels.
     */
    public int getFrameWidth()
    {
        return wizardWidth;
    }

    /**
     * Returns the height of the wizard window as configured in
     * the wizard.xml configuration.
     * 
     * @return Height of the window in pixels.
     */
    public int getFrameHeight()
    {
        return wizardHeight;
    }

    /**
     * Returns the map of external tools.
     * @return Map of external tools.
     */
    public Map getExternalTools()
    {
        return externalToolsMap;
    }
    
    /**
     * Returns an instance of a DoctorCommunicator referenced by name
     * as configured in the configuration xml.
     * 
     * @param name Name of the DoctorCommunicator.
     * @return Instance of the Communicator.
     */
    public DoctorCommunicator getCommunicatorByName(String name)
    {
        return (DoctorCommunicator)communicatorsMap.get(name);
    }
}
