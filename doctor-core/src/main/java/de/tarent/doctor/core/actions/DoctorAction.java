/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 14.10.2005
 */

package de.tarent.doctor.core.actions;

import java.util.Map;

import de.tarent.doctor.core.WizardData;

/**
 * Represents an action that can be invoked inbetween show-panel commands.
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public interface DoctorAction
{
    /**
     * Invokes this action.
     * 
     * @param wizardData The current wizardData instance modified by this wizard run.
     */
    public void invoke(WizardData wizardData);

    /**
     * Sets the runtime parameters. This is called with a map containing
     * the key-value pairs from the configuration file.
     * 
     * @param parameters Map with parameters.
     */
    public void setParameters(Map parameters);
}
