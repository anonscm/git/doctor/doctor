/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 4.10.2005
 */

package de.tarent.doctor.core.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;

import com.jgoodies.looks.Options;
import com.jgoodies.looks.plastic.PlasticLookAndFeel;
import com.jgoodies.looks.plastic.PlasticTheme;

import de.tarent.commons.utils.Config;
import de.tarent.commons.utils.Log;
import de.tarent.doctor.core.ExternalTool;
import de.tarent.doctor.core.FlowParser;
import de.tarent.doctor.core.PanelRegistryEntry;
import de.tarent.doctor.core.Wizard;
import de.tarent.doctor.core.WizardData;

/**
 * This class represents the frame for the tarent Doctor wizard UI. It
 * defines the window, controls the buttons and all widgets that are
 * not in the panels area.
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public class WizardFrame extends JFrame
{
    /** serialVersionUID */
	private static final long serialVersionUID = 7324159445958553133L;
	private JPanel navigationButtonFrame = null;
    private JPanel auxiliaryButtonFrame = null;
    private JPanel contentFrame = null;
    private JPanel panelFrame = null;
    private JPanel buttonFrame = null;
    private JMenuBar mainMenuBar = null;
    private JMenu doctorMenu = null;
    private JMenu editMenu = null;
    private JMenu toolsMenu = null;
    private JMenu helpMenu = null;
    private JMenuItem loadDataMenuItem = null;
    private JMenuItem saveDataMenuItem = null;
    private JMenuItem aboutMenuItem = null;
    private JMenuItem cutMenuItem = null;
    private JMenuItem copyMenuItem = null;
    private JMenuItem pasteMenuItem = null;
    private JButton nextButton = null;
    private JButton previousButton = null;
    private JButton helpButton = null;    
    private JButton finishButton;
    
    private Font defaultFont = null;
    
    private WizardPanelControl currentPanel = null;
    private WizardData wizardData = null;
    private Wizard parent = null;
    private FlowParser parser = null;

    private class FrameListener implements ActionListener
    {
        /**
         * This method is called when an event occurs in the
         * wizard frame - eg. one of the frame buttons (next, previous,
         * help, finish) is pressed or one of the menus is selected.
         * 
         * @param e Swing action event that was triggered.
         */
        public void actionPerformed(ActionEvent e)
        {
            if (getNextButton().equals(e.getSource()))
            {
                // Display next panel                
                PanelRegistryEntry nextPanel = parser.next();
                if (nextPanel!=null) displayPanel(nextPanel);
            }            
            else if (getPreviousButton().equals(e.getSource()))
            {
                // Display previous panel
                PanelRegistryEntry nextPanel = parser.previous();
                if (nextPanel!=null) displayPanel(nextPanel);
            }            
            else if (getFinishButton().equals(e.getSource()))
            {
                // UI run is finished: trigger B2B function
                parent.UIFinished();
            }            
            else if (getHelpButton().equals(e.getSource()))
            {
                // Display help window
                new HelpDialog(WizardFrame.this, Messages.getString("WizardFrame.DialogTitleHelp"), true, parser.getHelpUrlByPanelName(currentPanel.getName())); //$NON-NLS-1$
            }            
        }
    }
    
    /**
     * Constructs a new WizardFrame instance, displays the main window,
     * gets the first panel from the parser and swallows it into
     * the main window. With this constructor, the default window
     * title from the message bundle is used.
     * 
     * @param parent Parent Wizard controlling component.
     * @param parser Configuration Parser.
     */
    public WizardFrame(Wizard parent, FlowParser parser, WizardData wizardData)
    {
        super();
        initialize(parent, parser, Messages.getString("WizardFrame.defaultWindowTitle"), wizardData); //$NON-NLS-1$
    }
    
    /**
     * Constructs a new WizardFrame instance, displays the main window,
     * gets the first panel from the parser and swallows it into
     * the main window.
     * 
     * @param parent Parent Wizard controlling component.
     * @param parser Configuration Parser.
     * @param windowTitle Title of the main window.
     */
    public WizardFrame(Wizard parent, FlowParser parser, String windowTitle, WizardData wizardData)
    {
        super();
        initialize(parent, parser, windowTitle, wizardData);
    }

    private void initialize(Wizard parent, FlowParser parser, String windowTitle, WizardData wizardData)
    {
        this.parent = parent;
        this.parser  = parser;
        
        this.wizardData = wizardData;
    
        // emulate modal window
        this.setAlwaysOnTop(true);
        
        // Setting LAF Options
        JFrame.setDefaultLookAndFeelDecorated(true);
        try 
        {
        	String lookAndFeel = Config.getConfig().getString("configuration.ui.lookandfeel");
        	
            if (lookAndFeel != null && lookAndFeel.startsWith("com.jgoodies"))
            {
                // We use a JGoodies LAF
                Options.setPopupDropShadowEnabled(true);
                UIManager.put("jgoodies.popupDropShadowEnabled", Boolean.TRUE);
                PlasticLookAndFeel laf = (PlasticLookAndFeel)Class.forName(Config.getConfig().getString("configuration.ui.lookandfeel")).newInstance();
                PlasticLookAndFeel.setMyCurrentTheme((PlasticTheme)Class.forName(Config.getConfig().getString("configuration.ui.colortheme")).newInstance());
                UIManager.setLookAndFeel(laf);
            }
            else if(lookAndFeel != null)
            {
                // We use one of the default Java LAFs
                UIManager.setLookAndFeel((LookAndFeel)Class.forName(Config.getConfig().getString("configuration.ui.lookandfeel")).newInstance());            
            }
        } 
        catch (Exception e) 
        { 
            throw new RuntimeException("Can't set look and feel \""+Config.getConfig().getString("configuration.ui.lookandfeel")+"\".");
        }
        
        // Setting font
        if (Config.getConfig().containsKey("configuration.ui.font.file"))
        {
	        File fontFile = new File(Config.getConfig().getString("configuration.ui.font.file"));
	        float fontSize = Double.valueOf(Config.getConfig().getString("configuration.ui.font.size")).floatValue();
	        String fontStyleName = Config.getConfig().getString("configuration.ui.font.style");
	        int fontStyle = -1;
	        try
	        {
	            fontStyle = Class.forName("java.awt.Font").getField(fontStyleName).getInt(null);
	        }
	        catch (Exception e)
	        {
	            Log.error(this.getClass(), "Can't set font style: " + fontStyleName, e);
	        }
	        if (fontFile.exists())
	        {
		        this.setWizardDefaultFont(fontFile, fontStyle, fontSize);
	        }
	        else
	        {
	        	Log.error(this.getClass(), "Defined font file do not exist.");
	        }
        } else {
            Log.debug(this.getClass(), "No font defined.");
        }
        
        // Additional Window properties
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(new Dimension(parser.getFrameWidth(),parser.getFrameHeight()));
        this.setContentPane(getContentFrame());
        this.setJMenuBar(getMainMenuBar());
        
        this.setTitle(windowTitle);
        this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/gfx/tarent.png")));
        
        this.nextButtonSetEnabled(false);
        this.previousButtonSetEnabled(true);
        this.finishButtonSetEnabled(false);

        // Display the first panel
        PanelRegistryEntry thisPanel = parser.next();
        displayPanel(thisPanel);
    }

    private void setWizardDefaultFont(File ttfFont, int style, float size)
    {
        try
        {
            this.setDefaultFont(Font.createFont(Font.TRUETYPE_FONT, ttfFont).deriveFont(style, size));
        }
        catch (FontFormatException e)
        {
            Log.error(this.getClass(), "Can't recognize font format of font " + ttfFont.getAbsolutePath());
            e.printStackTrace();
        }
        catch (IOException e)
        {
            Log.error(this.getClass(), "Can't read font " + ttfFont.getAbsolutePath());
            e.printStackTrace();
        }
        
        Enumeration en = UIManager.getDefaults().keys();
        while(en.hasMoreElements())
        {
          Object key = en.nextElement();
          Object value = UIManager.get(key);
          if (value instanceof Font)
          {
            UIManager.put( key, this.getDefaultFont());
          }
        }
    }
    
    /**
     * Displays the given panel. This method removes the current panel
     * from the frame and displays the panel represented by the given
     * PanelRegistryEntry. It also manages the buttons in the frame
     * based on the panel's state.
     * 
     * @param panelEntry The panel to be displayed.
     */
    public void displayPanel(PanelRegistryEntry panelEntry)
    {
        // If there is a panel currently displayed, remove it
        if (currentPanel!=null)
        {
            this.remove((Component)currentPanel.getUI());
            currentPanel.dispose();
            currentPanel = null;
        }
        
        // Now get the new panel and display it
        currentPanel = panelEntry.getController();
        currentPanel.initialize(this, wizardData, panelEntry.getParameters());
        this.getPanelFrame().add((Component)currentPanel.getUI());
               
        this.validatedStatusUpdated();
    }
    
    private JPanel getContentFrame()
    {
        if (contentFrame == null)
        {
            contentFrame = new JPanel();
            contentFrame.setLayout(new BorderLayout());
            contentFrame.add(getPanelFrame(), BorderLayout.CENTER);
            contentFrame.add(getButtonFrame(), BorderLayout.SOUTH);
        }
        return contentFrame;
    }

    private JPanel getButtonFrame()
    {
        if (buttonFrame == null)
        {
            buttonFrame = new JPanel();
            buttonFrame.setLayout(new BorderLayout());
            buttonFrame.add(getAuxiliaryButtonFrame(), BorderLayout.WEST);
            buttonFrame.add(getNavigationButtonFrame(), BorderLayout.EAST);
        }
        return buttonFrame;
    }

    private JPanel getPanelFrame()
    {
        if (panelFrame == null)
        {
            panelFrame = new JPanel();
            panelFrame.setLayout(new BorderLayout());
        }
        return panelFrame;
    }

    private JPanel getAuxiliaryButtonFrame()
    {
        if (auxiliaryButtonFrame == null)
        {
            auxiliaryButtonFrame = new JPanel();
            FlowLayout layout = new FlowLayout();
            layout.setHgap(20);
            layout.setVgap(20);
            layout.setAlignment(FlowLayout.RIGHT);
            auxiliaryButtonFrame.setLayout(layout);
            auxiliaryButtonFrame.add(getHelpButton());
        }
        return auxiliaryButtonFrame;
    }
    
    private JPanel getNavigationButtonFrame()
    {
        if (navigationButtonFrame == null)
        {
            navigationButtonFrame = new JPanel();
            //GridLayout layout = new GridLayout(1, 3);
            BoxLayout layout = new BoxLayout(navigationButtonFrame, BoxLayout.LINE_AXIS);
            //layout.setHgap(20);
            //layout.setVgap(20);
            
            //navigationButtonFrame.add(Box.createHorizontalGlue());
            navigationButtonFrame.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
            //layout.setAlignment(FlowLayout.RIGHT);
            navigationButtonFrame.setLayout(layout);
            navigationButtonFrame.add(getFinishButton());
            navigationButtonFrame.add(Box.createRigidArea(new Dimension(20, 0)));
            navigationButtonFrame.add(getPreviousButton());
            navigationButtonFrame.add(Box.createRigidArea(new Dimension(20, 0)));
            navigationButtonFrame.add(getNextButton());
        }
        return navigationButtonFrame;
    }
    
    /**
     * Enables or disables the finished button. 
     * 
     * @param state True enables the button, false disables it.
     */
    public void finishButtonSetEnabled(boolean state)
    {
        this.getFinishButton().setEnabled(state);
    }
    
    private JButton getFinishButton()
    {
        if (finishButton == null)
        {
            finishButton = new JButton(Messages.getString("WizardFrame.ButtonTextFinish")); //$NON-NLS-1$
            finishButton.setIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/gfx/key_enter.png"))));
            finishButton.setHorizontalTextPosition(JButton.LEFT);
            finishButton.setIconTextGap(6);
            finishButton.addActionListener(new FrameListener());
            finishButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 10), "enterPressed");
            finishButton.getActionMap().put("enterPressed", new AbstractAction() {
                public void actionPerformed(ActionEvent e) {
                    finishButton.doClick(100);
                }
            });
        }
        return finishButton;
    }

    /**
     * Enables or disables the next button. 
     * 
     * @param state True enables the button, false disables it.
     */
    public void nextButtonSetEnabled(boolean state)
    {
        getNextButton().setEnabled(state);
    }
    
    private JButton getNextButton()
    {
        if (nextButton == null)
        {
            nextButton = new JButton(Messages.getString("WizardFrame.ButtonTextNext")); //$NON-NLS-1$
            nextButton.setIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/gfx/1rightarrow.png"))));
            nextButton.setHorizontalTextPosition(JButton.LEFT);
            nextButton.setIconTextGap(6);
            nextButton.addActionListener(new FrameListener());
            nextButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 10), "rightPressed");
            nextButton.getActionMap().put("rightPressed", new AbstractAction() {
                public void actionPerformed(ActionEvent e) {
                	nextButton.doClick(100);
                }
            });
        }
        return nextButton;
    }
    
    /**
     * Enables or disables the previous button. 
     * 
     * @param state True enables the button, false disables it.
     */
    public void previousButtonSetEnabled(boolean state)
    {
        getPreviousButton().setEnabled(state);
    }

    private JButton getPreviousButton()
    {
        if (previousButton == null)
        {
            previousButton = new JButton(Messages.getString("WizardFrame.ButtonTextPrevious")); //$NON-NLS-1$
            previousButton.setIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/gfx/1leftarrow.png"))));
            previousButton.addActionListener(new FrameListener());
            previousButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 10), "leftPressed");
            previousButton.getActionMap().put("leftPressed", new AbstractAction() {
                public void actionPerformed(ActionEvent e) {
                	previousButton.doClick(100);
                }
            });
        }
        return previousButton;
    }

    private JButton getHelpButton()
    {
        if (helpButton == null)
        {
            helpButton = new JButton(Messages.getString("WizardFrame.ButtonTextHelp")); //$NON-NLS-1$
            helpButton.setIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/gfx/help.png"))));
            helpButton.addActionListener(new FrameListener());
            helpButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0), "f1Pressed");
            helpButton.getActionMap().put("f1Pressed", new AbstractAction() {
                public void actionPerformed(ActionEvent e) {
                	helpButton.doClick(100);
                }
            });
        }
        return helpButton;
    }

    private JMenuBar getMainMenuBar()
    {
        if (mainMenuBar == null)
        {
            mainMenuBar = new JMenuBar();
            mainMenuBar.add(getDoctorMenu());
            mainMenuBar.add(getEditMenu());
            if(getToolsMenu() != null)
            	mainMenuBar.add(getToolsMenu());
            mainMenuBar.add(getHelpMenu());
            mainMenuBar.add(Box.createHorizontalGlue());
            mainMenuBar.add(new JLabel(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/gfx/tarent_very_small.png")))));
        }
        return mainMenuBar;
    }

    private JMenu getDoctorMenu()
    {
        if (doctorMenu == null)
        {
            doctorMenu = new JMenu();
            
            doctorMenu.setText(Messages.getString("WizardFrame.MenuTextDoctor")); //$NON-NLS-1$
            doctorMenu.add(getLoadDataMenuItem());
            doctorMenu.add(getSaveDataMenuItem());
            doctorMenu.add(getPasteMenuItem());
        }
        return doctorMenu;
    }

    private JMenu getEditMenu()
    {
        if (editMenu == null)
        {
            editMenu = new JMenu();
            editMenu.setText(Messages.getString("WizardFrame.MenuTextEdit")); //$NON-NLS-1$
            editMenu.add(getCutMenuItem());
            editMenu.add(getCopyMenuItem());
            editMenu.add(getPasteMenuItem());
        }
        return editMenu;
    }

    private JMenu getToolsMenu()
    {
        if (toolsMenu == null)
        {
        	final Map tools = parser.getExternalTools();
        	
        	if(tools.isEmpty()) return null;
        	
            toolsMenu = new JMenu();
            toolsMenu.setText(Messages.getString("WizardFrame.MenuTextTools")); //$NON-NLS-1$

            ActionListener toolsListener = new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    String name = e.getActionCommand();
                    ExternalTool thisTool = (ExternalTool)tools.get(name);
                    thisTool.execute(wizardData);
                }               
            };
            
            Iterator iter = tools.values().iterator();
            
            while (iter.hasNext())
            {
                ExternalTool thisTool = (ExternalTool)iter.next();

                JMenuItem thisMenuItem = new JMenuItem();
                thisMenuItem.setText(thisTool.getName());
                thisMenuItem.addActionListener(toolsListener);
                
                toolsMenu.add(thisMenuItem);
            }
        }
        return toolsMenu;
    }

    private JMenu getHelpMenu()
    {
        if (helpMenu == null)
        {
            helpMenu = new JMenu();
            helpMenu.setText(Messages.getString("WizardFrame.MenuTextHelp")); //$NON-NLS-1$
            helpMenu.add(getAboutMenuItem());
        }
        return helpMenu;
    }

    private JMenuItem getAboutMenuItem()
    {
        if (aboutMenuItem == null)
        {
            aboutMenuItem = new JMenuItem();
            aboutMenuItem.setText(Messages.getString("WizardFrame.MenuTextAbout")); //$NON-NLS-1$
            aboutMenuItem.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    new AboutDialog(WizardFrame.this, Messages.getString("WizardFrame.DialogTitleAbout"), true); //$NON-NLS-1$
                }
            });
        }
        return aboutMenuItem;
    }

    private JMenuItem getLoadDataMenuItem()
    {
        if (loadDataMenuItem == null)
        {
            loadDataMenuItem = new JMenuItem();
            loadDataMenuItem.setText(Messages.getString("WizardFrame.MenuTextLoadData")); //$NON-NLS-1$
            loadDataMenuItem.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    JFileChooser fc = new JFileChooser();
                    /*int returnVal =*/ fc.showOpenDialog(WizardFrame.this);
                    File selectedFile = fc.getSelectedFile();
                    if (selectedFile==null)
                    {
                        Log.info(this.getClass(), "Loading aborted.");
                        return;
                    }

                    wizardData.loadFromFile(selectedFile);
                    Log.info(this.getClass(), "Loaded data from " + selectedFile.getAbsolutePath());
                }
            });
        }
        return loadDataMenuItem;
    }

    private JMenuItem getSaveDataMenuItem()
    {
        if (saveDataMenuItem == null)
        {
            saveDataMenuItem = new JMenuItem();
            saveDataMenuItem.setText(Messages.getString("WizardFrame.MenuTextSaveData")); //$NON-NLS-1$
            saveDataMenuItem.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    JFileChooser fc = new JFileChooser();
                    /*int returnVal =*/ fc.showSaveDialog(WizardFrame.this);
                    File selectedFile = fc.getSelectedFile();
                    if (selectedFile==null)
                    {
                        Log.info(this.getClass(), "Saving aborted.");
                        return;
                    }

                    wizardData.saveToFile(selectedFile);
                    Log.info(this.getClass(), "Saved data to " + selectedFile.getAbsolutePath());
                }
            });
        }
        return saveDataMenuItem;
    }

    private JMenuItem getCutMenuItem()
    {
        if (cutMenuItem == null)
        {
            cutMenuItem = new JMenuItem();
            cutMenuItem.setText(Messages.getString("WizardFrame.MenuTextCut")); //$NON-NLS-1$
            cutMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X,
                    Event.CTRL_MASK, true));
        }
        return cutMenuItem;
    }

    private JMenuItem getCopyMenuItem()
    {
        if (copyMenuItem == null)
        {
            copyMenuItem = new JMenuItem();
            copyMenuItem.setText(Messages.getString("WizardFrame.MenuTextCopy")); //$NON-NLS-1$
            copyMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C,
                    Event.CTRL_MASK, true));
        }
        return copyMenuItem;
    }

    private JMenuItem getPasteMenuItem()
    {
        if (pasteMenuItem == null)
        {
            pasteMenuItem = new JMenuItem();
            pasteMenuItem.setText(Messages.getString("WizardFrame.MenuTextPaste")); //$NON-NLS-1$
            pasteMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V,
                    Event.CTRL_MASK, true));
        }
        return pasteMenuItem;
    }

    /**
     * This method is called when the validated state of the current panel
     * changes. It takes care of enabling/disabling the next button based
     * on the validation status.
     */
    public void validatedStatusUpdated()
    {
    	showRelevantButtons(parser.isLastEntry(), parser.isFirstEntry());
    	
    	getNextButton().setEnabled(this.currentPanel.isValidated());
    	getFinishButton().setEnabled(this.currentPanel.isValidated());
    	
    }
    
    public void showRelevantButtons(boolean pLastEntry, boolean pFirstEntry)
    {
    	getNextButton().setVisible(!pLastEntry);
    	getFinishButton().setVisible(pLastEntry);
    	getPreviousButton().setVisible(!pFirstEntry);
    }
    
    /**
     * Gets the default font used by all widgets.
     * 
     * @return Font
     */
    public Font getDefaultFont()
    {
        return defaultFont;
    }

    /**
     * Sets the default font used by all widgets.
     * 
     * @param defaultFont Font
     */
    public void setDefaultFont(Font defaultFont)
    {
        this.defaultFont = defaultFont;
    }
} 