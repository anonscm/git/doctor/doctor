/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 12.10.2005
 */

package de.tarent.doctor.core.ui;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 * The step entry class represents one step in the current
 * history of steps this wizard has performed. It is used
 * by the history feature that allows going back to the
 * previous panel without "back-calculating" the environment
 * before the current panel. This is needed because of the
 * arbitrary steps that can be taken between panel displays 
 * in the form of actions. 
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public class StepEntry
{
    private Node contextNode = null;
    
    /**
     * Constructs a new StepEntry.
     * 
     * @param contextNode The starting context node of this step.
     */
    public StepEntry(Node contextNode)
    {
        this.contextNode = contextNode;
    }
    
    /**
     * Returns the context node that was used just prior to the
     * execution of the panel represented by this StepEntry.
     * 
     * @return Context node of this StepEntry.
     */
    public Node getContextNode()
    {
        return contextNode;
    }
    
    /**
     * Returns a string representation of this StepEntry.
     */
    public String toString()
    {
        if (contextNode.getNodeType()==Node.ELEMENT_NODE)
        {
            NamedNodeMap attributes = ((Element)contextNode).getAttributes();
            String out = contextNode.getNodeName() + "( ";
            for (int i=0; i<attributes.getLength(); i++)
                out += attributes.item(i).getNodeName() + "=" + attributes.item(i).getNodeValue() + " ";
            return out + ")";
        }
        else
            return contextNode.getNodeName();
    }
}
