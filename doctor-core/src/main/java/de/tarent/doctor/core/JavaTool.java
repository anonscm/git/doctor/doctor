/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 05.12.2005
 */

package de.tarent.doctor.core;

import de.tarent.commons.utils.Log;
import de.tarent.doctor.core.tools.Tool;

public class JavaTool extends ExternalTool
{
    private Tool toolInstance = null;
    
    /**
     * Creates a new java-based ExternalTool.
     * 
     * @param name Name of the tool. This String will also be displayed in the WizardFrame.
     * @param className Name of the Tool class. 
     */
    public JavaTool(String name, String className)
    {
        this.setName(name);
        try
        {
            toolInstance = (Tool)Class.forName(className).newInstance();
        }
        catch (InstantiationException e)
        {
            Log.error(this.getClass(), "Can't instantiate external tool: " + className);
            return;
        }
        catch (IllegalAccessException e)
        {
            Log.error(this.getClass(), "Illegal access to external tool: " + className);
            return;
        }
        catch (ClassNotFoundException e)
        {
            Log.error(this.getClass(), "Can't find class of external tool: " + className);
            return;
        }
    }
    
    public Tool getToolInstance()
    {
        return toolInstance;
    }

    public void setToolInstance(Tool toolInstance)
    {
        this.toolInstance = toolInstance;
    }

    public void execute(WizardData wizardData)
    {
        if (this.getName()==null || this.getToolInstance()==null)
        {
            Log.error(this.getClass(), "Can't invoke external tool: name, ad instance have to be present.");
            return;
        }        
        
        if (!toolInstance.run(wizardData))
            Log.error(this.getClass(), "Invoke external tool failed: tool returned false.");
    }
}
