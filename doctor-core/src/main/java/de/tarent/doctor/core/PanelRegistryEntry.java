/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 10.10.2005
 */

package de.tarent.doctor.core;

import java.util.Map;

import de.tarent.doctor.core.ui.WizardPanelControl;

/**
 * This class stores data of a configured panel. For each panel that is
 * defined in the configuration, one instance of this class is created
 * and stored in the parser for later reference.
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public class PanelRegistryEntry
{
    private String name;
    private WizardPanelControl controller;
    private Map parameters;
    private boolean lastPanel = false;
    private String helpFileName = null;
    
    /**
     * Constructs a new PanelRegistryEntry and tries to get an instance of the
     * configured controller class.
     * 
     * @param pName Name of the Entry.
     * @param pController Name of the controller class.
     * @param pParameters Parameters for this panel as configured in the configuration.
     * @param pLastPanel True if this panel was marked as last in the configuration, true otherwise.
     * @param pHelpFileName URL to help HTML file.
     */
    public PanelRegistryEntry(String pName, String pController, Map pParameters, boolean pLastPanel, String pHelpFileName)
    {
        name = pName;
        lastPanel = pLastPanel;
        helpFileName = pHelpFileName;
        
        try
        {
            controller = (WizardPanelControl) Class.forName(pController).newInstance();
            controller.setParameters(pParameters);
            controller.setName(pName);
        }
        catch (InstantiationException e)
        {
            throw new RuntimeException("Maybe specified controller-file is not a valid WizardPanelControl", e);
        }
        catch (IllegalAccessException e)
        {
            throw new RuntimeException(e);
        }
        catch (ClassNotFoundException e)
        {
            throw new RuntimeException(e);
        }
        
        parameters = pParameters;
    }

    /**
     * Returns the instance of the controller class.
     * 
     * @return The controller object of this panel.
     */
    public WizardPanelControl getController()
    {
        return controller;
    }

    /**
     * Returns the symbolic name of this panel.
     * 
     * @return Name of this panel.
     */
    public String getName()
    {
        return name;
    }

    /**
     * Returns the configured parameters.
     * 
     * @return Configured parameters.
     */
    public Map getParameters()
    {
        return parameters;
    }

    /**
     * Returns whether this panel was marked last in the configuration.
     * 
     * @return True if the panel was marked last, false otherwise.
     */
    public boolean isLastPanel()
    {
        return lastPanel;
    }

    /**
     * Returns the URL to the help HTML file of this panel.
     * 
     * @return Returns the helpURL.
     */
    public String getHelpFileName()
    {
        return helpFileName;
    }

    /**
     * Returns a string representation of this Entry.
     */
    public String toString()
    {
        return name + " (" + parameters + ")";
    }
}
