/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 13.10.2005
 */

package de.tarent.doctor.core.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import de.tarent.commons.utils.Version;

/**
 * This class displays the about dialog.
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public class AboutDialog extends JDialog
{
    /** serialVersionUID */
	private static final long serialVersionUID = 467563582557310268L;
	private JPanel jContentPane = null;
    private JButton jButton = null;
    private JPanel jPanel = null;

    private class ImagePanel extends JPanel
    {
        /** serialVersionUID */
		private static final long serialVersionUID = 248393103127120730L;

		protected void paintComponent(Graphics g) 
        {
            super.paintComponent(g);

            Image aboutImage = null;
            /*
            try 
            {
                aboutImage = javax.imageio.ImageIO.read(new URL("file:gfx/about.png"));
            } 
            catch (IOException ioe) 
            {
                throw new RuntimeException("Can't open about image.");
            }
            */
            
            aboutImage = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/gfx/about.png"));

            Graphics2D g2d = (Graphics2D)g;
            g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                    RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g2d.setRenderingHint(RenderingHints.KEY_RENDERING,
                    RenderingHints.VALUE_RENDER_QUALITY);

            g.drawImage(aboutImage, 0, 0, this);
            g.setColor(new Color(255,255,255));
            g.drawString("Release " + Version.getVersion(), 10, 407);
            g.drawString(Version.getCopyright(), 10, 422);
            g.drawString(Version.getExtModules(), 10, 437);
        }
    }
    
    /**
     * Constructs a new about dialog. It displays the image in gfx/about.png and the given
     * string data.
     * 
     * @param owner Owning frame.
     * @param title Title of the about window.
     * @param modal True if the dialog should be modal, false otherwise.
     */
    public AboutDialog(Frame owner, String title, boolean modal)
    {
        super(owner, title, modal);
        initialize();
    }

    private void initialize()
    {
        this.setContentPane(getJContentPane());
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    private JPanel getJContentPane()
    {
        if (jContentPane == null)
        {
            jContentPane = new JPanel();
            jContentPane.setLayout(new BorderLayout());
            jContentPane.add(getJButton(), java.awt.BorderLayout.SOUTH);
            jContentPane.add(getJPanel(), java.awt.BorderLayout.CENTER);
        }
        return jContentPane;
    }

    private JButton getJButton()
    {
        if (jButton == null)
        {
            jButton = new JButton();
            jButton.setText(Messages.getString("Buttons.Close"));
            jButton.addActionListener(new java.awt.event.ActionListener()
            {
                public void actionPerformed(java.awt.event.ActionEvent e)
                {
                    AboutDialog.this.setVisible(false);
                    AboutDialog.this.dispose();
                }
            });
            jButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "escapePressed");
            jButton.getActionMap().put("escapePressed", new AbstractAction() {
                public void actionPerformed(ActionEvent e) {
                	jButton.doClick(100);
                }
            });
        }
        return jButton;
    }

    private JPanel getJPanel()
    {
        if (jPanel == null)
        {
            jPanel = new ImagePanel();
            jPanel.setPreferredSize(new Dimension(356, 446));
        }
        return jPanel;
    }

}  //  @jve:decl-index=0:visual-constraint="480,42"
