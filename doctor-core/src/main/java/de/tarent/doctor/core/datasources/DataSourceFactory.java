/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 12.10.2005
 */

package de.tarent.doctor.core.datasources;

import java.util.Map;

import de.tarent.doctor.core.FlowParser;

/**
 * This is a static wrapper for providing all parts of a Doctor wizard
 * easy access to the DataSources.
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public class DataSourceFactory
{
    private static Map dataSourceMap = null;
    
    static
    {
        dataSourceMap = FlowParser.getDataSources();
        if (dataSourceMap==null)
            throw new RuntimeException("DataSourceFactory called before config was parsed.");
    }

    /**
     * Returns the DataSource represented by the given name in the configuration.
     * 
     * @param name Name of the DataSource from the configuration file.
     * @return DataSource instance.
     */
    public static DoctorDataSource getByName(String name)
    {
        return (DoctorDataSource)dataSourceMap.get(name);
    }
}
