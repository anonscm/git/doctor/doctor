/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 05.12.2005
 */

package de.tarent.doctor.core;

import java.io.File;
import java.io.IOException;

import de.tarent.commons.utils.Log;

public class ShellTool extends ExternalTool
{
    private String command = null;
    private String workingDir = null;

    /**
     * Creates a new shell-based ExternalTool.
     * 
     * @param name Name of the tool. This String will also be displayed in the WizardFrame.
     * @param command Commandline to be executed when the tool starts.
     * @param workingDir Working dir that is set when executing this tool.
     */
    public ShellTool(String name, String command, String workingDir)
    {
        this.setName(name);
        this.setCommand(command);
        this.setWorkingDir(workingDir);
    }

    /**
     * Starts this exernal tool.
     */
    public void execute(WizardData wizardData)
    {
        if (this.getName()==null || this.getCommand()==null || this.getWorkingDir()==null)
        {
            Log.error(this.getClass(), "Can't invoke external tool: name, command and working directory have to be present.");
            return;
        }
        
        try
        {
            Runtime.getRuntime().exec(this.getCommand(), null, new File(this.workingDir));
        }
        catch (IOException e)
        {
            Log.error(this.getClass(), "Failed invoking external tool.", e);
        }
    }
    
    public String getCommand()
    {
        return command;
    }

    public void setCommand(String command)
    {
        this.command = command;
    }

    public String getWorkingDir()
    {
        return workingDir;
    }

    public void setWorkingDir(String workingDir)
    {
        this.workingDir = workingDir;
    }
}
