/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 05.12.2005
 */

package de.tarent.doctor.core.comm;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * This has to be implemented by all classes that want to act as
 * communication stubs to external systems like workflow engines.
 * <p>
 * A DoctorCommunicator sends the document that is being worked
 * on to an external service over an arbitrary communications protocol
 * that has to be handled completely in the DoctorCommunicator. With
 * this mechanism, the document can be sent for example via email or SOAP to
 * external receivers that work on the document after being created with 
 * Doctor.
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public abstract class DoctorCommunicator
{
    private Map parameters = null;
    
    /**
     * Sets the parameters.
     * 
     * @param parameters Parameters of this instance.
     */
    public void setParameters(Map parameters)
    {
        this.parameters = parameters;
    }
    
    /**
     * Gets the parameters.
     * 
     * @return Parameters of this instance.
     */
    public Map getParameters()
    {
        return this.parameters;
    }
    
    /**
     * Sends the given document to an external system like a
     * workflow system or an email box.
     * 
     * @param document Document that should be sent.
     * @param metaData additional meta data that should be sent. Can be null.
     */
    public abstract void sendDocument(File document, Map metaData) throws IOException;    
}

