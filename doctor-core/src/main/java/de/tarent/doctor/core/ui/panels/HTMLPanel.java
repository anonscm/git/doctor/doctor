/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 06.10.2005
 */

package de.tarent.doctor.core.ui.panels;

import java.awt.Component;
import java.awt.GridLayout;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.Option;

import de.tarent.commons.ui.JHTMLEditorKit;
import de.tarent.commons.ui.JHTMLPanel;
import de.tarent.commons.utils.Log;
import de.tarent.doctor.core.FlowParser;
import de.tarent.doctor.core.ui.PanelEvent;
import de.tarent.doctor.core.ui.WizardPanelControl;
import de.tarent.doctor.core.ui.WizardPanelUI;

/**
 * The HTMLPanel is a special panel class that uses a extended version of HTML
 * to render the panel's UI. The HTML file to use is configured using
 * the runtime parameters of the panel in the configuration.
 * <p>
 * To use this feature, follow this steps:
 * 
 * <ol>
 * <li>Define a custom widget in the configuration:<br>
 *     <code>
 *     &lt;htmlwidgets&gt;
 *         &lt;widget type="jslider" class="javax.swing.JSlider"/&gt;
 *     &lt;/htmlwidgets&gt;
 *     </code>
 * <li>Use the widget in your panel's html code:<br>
 *     <code>
 *     &lt;input type="jslider" name="widgetname"&gt;
 *     </code>
 * <li>Subclass this class and implement configureComponent(), getProperty() and setProperty().
 * <li>Implement a controller class by inheriting from WizardPanelControl.
 * <li>Register your panel in the configuration, put it into a flow and start the wizard.
 * </ol>
 * 
 * This class already supports some standard components in terms of getProperty() and
 * setProperty(). 
 * 
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public abstract class HTMLPanel extends JPanel implements WizardPanelUI, JHTMLPanel
{
    private WizardPanelControl controller = null;
    private JScrollPane scrollPane;
    private HTMLEditorKit htmlEditorKit;
    private Map componentMap = null;
    private String htmlFile = null;
    
    /**
     * Constructs a new HTML panel.
     * 
     * @param pController Controller for this panel.
     * @param pHtmlFile HTML source for constucting the UI.
     */
    public HTMLPanel(WizardPanelControl pController, String pHtmlFile)
    {
        super();
        controller = pController;
        componentMap = new HashMap();
        htmlFile = pHtmlFile;
        initialize();
    }

    private void initialize()
    {
        // Construct GUI   
        this.setLayout(new GridLayout(1,1));
        
        // Get HTML widget
        this.add(getHTMLPane());        
    }
    
    private JScrollPane getHTMLPane()
    {
        if (scrollPane == null)
        {            
            JEditorPane pane = new JEditorPane();
            pane.setContentType("text/html");
            htmlEditorKit = new JHTMLEditorKit(this, FlowParser.getHTMLWidgets());
            pane.setEditorKit(htmlEditorKit);
            pane.setFocusable(false);
            pane.setEditable(false);
            try
            {
            	if(htmlFile != null)
            		pane.setPage(getClass().getResource(htmlFile));
            }
            catch (IOException e)
            {
                e.printStackTrace();
                throw new RuntimeException("Can't initialize GUI html-file: " + htmlFile);
            }
            pane.setBackground(UIManager.getColor("Label.background"));
                        
            pane.setBorder(BorderFactory.createEmptyBorder());
            scrollPane = new JScrollPane(pane);
            scrollPane.setBorder(BorderFactory.createEmptyBorder());
        }
        return scrollPane;
    }

    /**
     * This is a callback called from the custom HTML renderer when a component
     * is actually created. This is used for syncronizing the rendering thread 
     * with the control (main) thread.
     * 
     * @param name Name of the component.
     * @param component Component that was created.
     */
    public void componentCreated(final String name, final Component component)
    {
        Log.debug(this.getClass(), "Component added to HTML panel: " + name + " as " + component.getClass());
        componentMap.put(name, component);
        
        this.configureComponent(name, component);        
    }
    
    /**
     * Returns the component registered under the given name. This is the main
     * way to access the components on a html panel. The name is the value of
     * the name attribute in the html source. If you need to read or set some
     * values or properties of a component, get the component by passing it's
     * name to this method.
     * 
     * @param name Name of the component.
     * @return Component.
     */
    protected Component getComponentByName(String name)
    {
        Component out = (Component)componentMap.get(name);
        if (out==null)
            throw new RuntimeException("Component not created yet: " + name);
        return out;
    }

    /**
     * Returns the current value of a component in the html panel. This
     * method has to be replaced in a subclass and should be called explicitly
     * by the overriding method to support standard components that are already
     * supported by this default implementation.
     * 
     * @param propertyName Name of the component as referenced by the name attribute in the html source.
     * @return Value of the property.
     */
    public Object getProperty(String propertyName)
    {
        Component component = getComponentByName(propertyName);
        
        if (component==null)
            throw new RuntimeException("Component is null:" + propertyName);
        
        // TODO: This has to be extended for the various standard html widgets
        if (component instanceof JTextField)
            return ((JTextField)component).getText();
        else if (component instanceof JButton)
            return ((JButton)component).getName();
        else if (component instanceof JComboBox)
        {
            Object selectedOption = ((JComboBox)component).getSelectedItem();
            if (selectedOption instanceof Option)
                return ((Option)selectedOption).getValue();
            else
                return selectedOption;
        }
        else
            return null;
    }
    
    /**
     * Sets the value of a component in the html panel. This
     * method has to be replaced in a subclass and should be called explicitly
     * by the overriding method to support standard components that are already
     * supported by this default implementation.
     * 
     * @param propertyName Name of the component as referenced by the name attribute in the html source.
     * @param value Value to be set.
     * @return true if the setting of the value was successful, false otherwise.
     */
    public boolean setProperty(String propertyName, Object value)
    {
        Component component = getComponentByName(propertyName);
        
        // TODO: This has to be extended for the various standard html widgets
        if (component instanceof JTextField)
        {
            ((JTextField)component).setText((String)value);
            return true;
        }
        
        return false;
    }

    /**
     * Sends an event to the controller instance.
     * 
     * @param eventType Type of event.
     * @param propertyName Name of the property in the WizardData that was touched by this event.
     * @param propertyValue Value of the property in the WizardData that was touched by this event.
     */
    protected void sendEvent(int eventType, String propertyName, Object propertyValue)
    {
        controller.propertyChanged(new PanelEvent(this, eventType, propertyName, propertyValue));
    }       
    
    // abstract methods follow
    
    /**
     * Configures a component. This is called when the component is actually created
     * by the rendering thread. Remember that all custom components are created 
     * as "default" with no additional configuration. If you want to set some properties
     * like size, color or other things, do it in this method.
     * 
     * @param name Name of the component.
     * @param component The component.
     */
    protected abstract void configureComponent(String name, Component component);
}

