/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 07.10.2005
 */

package de.tarent.doctor.core;

import de.tarent.commons.utils.Config;
import de.tarent.commons.utils.Log;

/**
 * Starts the tarent Doctor.
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public class Startup
{
    /**
     * This method starts the tarent Doctor. The configuration file
     * is given as the first program argument and the flow to be
     * executed as the second argument.
     * <p>
     * If the third argument is present, it is parsed as a pre-defined
     * set of key-value pairs for the wizardData instance. If this argument is
     * not null, the UI will be skipped, directly running the engine.
     * 
     * @param args Commandline arguments.
     */
    public static void main(String[] args)
    {
        Log.info(Startup.class, "tarent Doctor Core starts...");

        Config.parse(args[0]);
        if(args.length > 3)
        	new Wizard(args[0], args[1], args[3], Boolean.valueOf(args[2]).booleanValue());
        if (args.length > 2)
            new Wizard(args[0], args[1], null, Boolean.valueOf(args[2]).booleanValue());
        else if(args.length > 1)
            new Wizard(args[0], args[1], null, false);
        else if(args.length <= 1)
        {
        	Log.info(Startup.class, "wrong arguments!\r\nproper use: Startup config-file flow [managedMode true/false] [datafile]");
        	System.exit(1);
        }
    }
}
