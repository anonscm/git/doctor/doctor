/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 04.11.2005
 */

package de.tarent.doctor.core.utils;

import java.util.Iterator;
import java.util.Properties;

import de.tarent.commons.ui.QualityFeedbackAgent;

public class DoctorQualityFeedbackAgent extends QualityFeedbackAgent
{
    /** serialVersionUID */
	private static final long serialVersionUID = -7997973097980908008L;

	public DoctorQualityFeedbackAgent(String pMessage, Exception pException)
    {
        super(pMessage, pException);
    }

    public void send(String pEmail, String pDoneText, String pMessage, Exception pException)
    {
    	String message = "From: " + pEmail + "\r\n";
    	message += "Error-Message: " + pMessage + "\r\n";
    	message += "What he/she was doing: " + pDoneText + "\r\n";
    	
    	// add system-properties
    	
    	message += "System-Properties:\r\n";
    	    	
    	Properties props = System.getProperties();
    	
    	Iterator it = props.keySet().iterator();
    	
    	while(it.hasNext())
    	{
    		String prop = (String)it.next();
    		message += prop + ": " + props.getProperty(prop) + "\r\n";
    	}
    	
    	
    	
    	try
    	{
//    		MailTool.getInstance().postMail(new String [] { "f.koester@tarent.de" }, "[Doctor Feedback]", message, "doctor-feedback@tarent.de");
    	}
    	catch(Exception pExcp)
    	{
    		pExcp.printStackTrace();
    	}
    }
}
