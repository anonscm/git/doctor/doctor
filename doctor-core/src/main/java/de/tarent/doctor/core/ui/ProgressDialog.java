/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 13.10.2005
 */

package de.tarent.doctor.core.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import de.tarent.doctor.core.Wizard;

/**
 * This class displays the progress dialog.
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public class ProgressDialog extends JDialog
{
    /** serialVersionUID */
	private static final long serialVersionUID = 2183094069527612040L;
	private JPanel jContentPane = null;
    private JProgressBar jProgressBar = null;
    private JTextArea jScrollPane = null;
    private Wizard wizard = null;
    private JPanel progressArea = null;
    private JButton cancelButton = null;

    /**
     * Constructs a new progress dialog. 
     * 
     * @param wizard Owning wizard.
     * @param title Title of the about window.
     */
    public ProgressDialog(Wizard wizard, String title)
    {
        this.setTitle(title);
        this.setModal(false);
        this.wizard = wizard;
        initialize();
    }

    private void initialize()
    {
        this.setContentPane(getJContentPane());
        this.pack();
        this.setLocationRelativeTo(null);
        this.setAlwaysOnTop(true);
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        this.setVisible(true);
    }

    private JPanel getJContentPane()
    {
        if (jContentPane == null)
        {
            jContentPane = new JPanel();
            jContentPane.setLayout(new BorderLayout());
            jContentPane.add(getProgressArea(), java.awt.BorderLayout.SOUTH);
            jContentPane.add(getJScrollPane(), java.awt.BorderLayout.CENTER);
        }
        return jContentPane;
    }

    private JPanel getProgressArea()
    {
        if (progressArea  == null)
        {
            progressArea = new JPanel();
            progressArea.setLayout(new BorderLayout());
            progressArea.add(getJProgressBar(), java.awt.BorderLayout.WEST);
            progressArea.add(getCancelButton(), java.awt.BorderLayout.EAST);
        }
        return progressArea;        
    }
    
    private JButton getCancelButton()
    {
        if (cancelButton   == null)
        {
            cancelButton = new JButton(Messages.getString("Buttons.Cancel"));
            cancelButton.addActionListener(new ActionListener() 
                    {
                        public void actionPerformed(ActionEvent e)
                        {
                            // Cancel was klicked
                            wizard.cancelEngine();
                        }
                    });
        }
        return cancelButton;                
    }
    
    public void setProgress(final int percent)
    {
        SwingUtilities.invokeLater(new Thread() 
        { 
            public void run()
            {
                jProgressBar.setValue(percent); 
                getJProgressBar().setStringPainted(true);
            }
        });
    }

    public void printMessage(final String message)
    {
        SwingUtilities.invokeLater(new Thread() 
                { 
                    public void run()
                    {
                        //TODO: Count lines here
                        jScrollPane.setRows(jScrollPane.getRows()+1);
                        jScrollPane.append(message + "\n");
                    }
                });
    }
    
    public void setIndeterminate(boolean pIndeterminate)
    {
    	getJProgressBar().setIndeterminate(pIndeterminate);
    	getJProgressBar().setStringPainted(false);
    }
    
    private JProgressBar getJProgressBar()
    {
        if (jProgressBar == null)
        {
            jProgressBar = new JProgressBar(0, 100);
            jProgressBar.setValue(0);
            jProgressBar.setStringPainted(true);            
        }
        return jProgressBar;
    }

    private JScrollPane getJScrollPane()
    {
        if (jScrollPane == null)
        {
            jScrollPane = new JTextArea();
            jScrollPane.setPreferredSize(new Dimension(500, 300));
            jScrollPane.setEditable(false);
            jScrollPane.setBorder(BorderFactory.createLineBorder(new Color(255,255,255), 10));
            jScrollPane.setFont(new Font("Fixed", Font.BOLD, 14));
        }
        return new JScrollPane(jScrollPane);
    }

}
