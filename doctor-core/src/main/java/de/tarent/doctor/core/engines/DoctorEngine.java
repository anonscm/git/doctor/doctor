/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 12.10.2005
 */

package de.tarent.doctor.core.engines;

import java.util.Map;

import de.tarent.commons.utils.Log;
import de.tarent.doctor.core.Wizard;
import de.tarent.doctor.core.WizardData;
import de.tarent.doctor.core.ui.ProgressDialog;

/**
 * Defines the interface for all Doctor engines.
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public abstract class DoctorEngine extends Thread
{
    private boolean isDefault = false;
    private Map parameters;
    private ProgressDialog progressReceiver = null;
    protected WizardData wizardData = null;
    private Wizard wizard = null;

    /**
     * Sets whether this engine was configured as default engine for the flow.
     * 
     * @param isDefault True if this engine was configured as default engine, false otherwise.
     */
    public void setDefault(boolean isDefault)
    {
        this.isDefault = isDefault;
    }

    /**
     * Returns whether this engine was configured as default engine for the flow.
     * 
     * @return True if this engine was configured as default engine, false otherwise.
     */
    public boolean isDefault()
    {
        return isDefault;
    }

    /**
     * Sets the runtime parameters. This is called with a map containing
     * the key-value pairs from the configuration file.
     * 
     * @param parameters Map with parameters.
     */
    public void setParameters(Map parameters)
    {
        this.parameters = parameters;
    }
    
    /**
     * Returns the config parameters.
     * 
     * @return Config parameters.
     */
    public Map getParameters()
    {
        return this.parameters;
    }
    
    /**
     * Inserts a message in the internal message buffer. This buffer
     * will be returned and resetted by calling getnextProgressMessage()
     * which is called from the progress window.
     * 
     * @param message One-line message to be appended.
     */
    protected void printMessage(String message)
    {
        progressReceiver.printMessage(message);
    }
    
    /**
     * Sets the progress of this engine in percent.
     * 
     * @param percent Progress in percent.
     */
    protected void setProgress(int percent)
    {
        progressReceiver.setProgress(percent);
    }
    
    protected void setProgressIndeterminate(boolean pIndeterminate)
    {
    	progressReceiver.setIndeterminate(pIndeterminate);
    }
    
    /**
     * Sets the progress receiver.
     * 
     * @param progressReceiver The receiver to receive progress information.
     */
    public void setProgressReceiver(ProgressDialog progressReceiver)
    {
        this.progressReceiver  = progressReceiver;
    }

    /**
     * Sets the WizardData to be processed by this engine.
     * 
     * @param wizardData WizardData to be processed.
     */
    public void setWizardData(WizardData wizardData)
    {
        this.wizardData  = wizardData;
    }

    /**
     * Sets the parent Wizard instance. This is needed for the
     * end-of-operations callback to the main Wizard.
     * 
     * @param wizard Wizard parent.
     */
    public void setWizardData(Wizard wizard)
    {
        this.wizard  = wizard;
    }    

    /**
     * Signals that this engine has finished operations. This must
     * be called as the last action in an engine.
     */
    protected void finished()
    {
        wizard.doctorFinished();
    }
    
    public void run()
    {
        try
        {
            this.process();
        }
        catch (Exception e)
        {
            Log.info(this.getClass(), "Engine was interrupted or has failed: " + e.getMessage());
            Log.debug(this.getClass(), e);
        }
    }
    
    // Abstract methods follow
    
    /**
     * Processes the given wizard data to produce the documents using
     * the GDG API. This method should also clean up resources when
     * finishing work.
     */
    public abstract void process() throws InterruptedException;
}
