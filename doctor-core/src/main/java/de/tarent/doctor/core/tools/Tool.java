/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 05.12.2005
 */

package de.tarent.doctor.core.tools;

import de.tarent.doctor.core.WizardData;

/**
 * This interface has to be implemented by all external Java-based tools
 * that should be selectable by the tools menu.
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public interface Tool
{
    /**
     * Runs the tool.
     * 
     * @param wizardData The current WizardData of this wizard.
     * @return True if execution was successful, false otherwise.
     */
    public boolean run(WizardData wizardData);
}
