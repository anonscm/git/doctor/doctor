/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 06.10.2005
 */

/**
 * This class defines the abstract parent class of all wizard panels. The
 * panels will be displayed in the content area of the wizard frame. They
 * contain all functional elements of a single page in the doctor wizard 
 * including the UI elements, the associated logic, and the mapping logic
 * for storing the results in a WizardData object.
 * 
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
package de.tarent.doctor.core.ui;

import java.util.Map;

import javax.swing.JPanel;

import de.tarent.doctor.core.WizardData;

/**
 * This class represents the controlling component of each wizard page.
 * It relies on a simple JPanel instance constructing the UI and 
 * providing a simple bean-like interface. This UI component can be
 * done using a GUI editor like the Eclipse VE. All functional things
 * like the B2B logic or the mapping to the WizardData has to be 
 * implemented in a subclass of this class.
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public abstract class WizardPanelControl
{
    /**
     * The controlled UI
     */
    private WizardPanelUI ui = null;
    
    /**
     * The WizardData instance holding the state information for all panels.
     */
    private WizardData wizardData = null;
    
    /**
     * The parent frame of this panel.
     */
    private WizardFrame parent = null;

    /**
     * Parameters for this panel. These are coming from the XML configuration.
     */
    private Map parameters = null;
    
    /**
     * If this variable is true, the user is allowed to step to the next page of the wizard.
     */
    private boolean isValidated = false;

    /**
     * Name of this panel
     */
    private String name = null;
    
    /**
     * Sets the UI for this page. This is called from the initialize() method
     * of the subclass while getting the panel instance.
     * 
     * @param ui
     */
    protected void setUI(WizardPanelUI ui)
    {
        this.ui=ui;
    }
    
    /**
     * Returns the JPanel UI component of this wizard page. This will be
     * called by the WizardFrame while swallowing the UI of this page into
     * the main window.
     * 
     * @return JPanel UI component of this wizard page.
     */
    public WizardPanelUI getUI()
    {
        return ui;
    }
    
    /**
     * Sets whether the user is allowed to step to the next page of the
     * wizard.
     * 
     * @param isValidated true if next is allowed, false otherwise.
     */
    protected void setValidated(boolean isValidated)
    {
        this.isValidated = isValidated;
        this.parent.validatedStatusUpdated();
    }
    
    /**
     * Returns true if the user is allowed to go to the next step of
     * the wizard. This will be set according to some logic implemented
     * in the B2B part of the current panel.
     * 
     * @return isValidated true if next is allowed, false otherwise.
     */
    public boolean isValidated()
    {
        return isValidated;
    }
    
    /**
     * Disposes this panel by removing all components from the 
     * underlying JPanel, setting it invisible and removing
     * the reference to it.
     */
    public void dispose()
    {
        // This assumes that WizardPanelUI is always derived from JPanel
        if (ui!=null)
        {
            ((JPanel)ui).setVisible(false);
            ((JPanel)ui).removeAll();
            ui = null;
        }
    }
    
    /**
     * Returns the associated WizardFrame instance that is the container
     * for this panel.
     * 
     * @return Parent WizardFrame instance
     */
    public WizardFrame getParent()
    {
        return parent;
    }

    /**
     * Sets the parent WizardFrame instance.
     * 
     * @param parent WizardFrame container for this panel.
     */
    public void setParent(WizardFrame parent)
    {
        this.parent = parent;
    }

    /**
     * Returns the WizardData map that serves as input and output data
     * for this panel.
     * 
     * @return WizardData i/o data.
     */
    public WizardData getWizardData()
    {
        return wizardData;
    }

    /**
     * Sets the WizardData map that serves as input and output data for
     * this panel.
     * 
     * @param wizardData WizardData i/o data.
     */
    public void setWizardData(WizardData wizardData)
    {
        this.wizardData = wizardData;
    }

    /**
     * Returns the parameters for this panel as configured in the 
     * configuration file.
     * 
     * @return Parameter map.
     */
    public Map getParameters()
    {
        return parameters;
    }

    /**
     * Sets the parameters for this panel as configured in the 
     * configuration file.
     * 
     * @param parameters Parameter map.
     */
    public void setParameters(Map parameters)
    {
        this.parameters = parameters;
    }

    /**
     * Returns the name of this panel.
     * 
     * @return Returns the name.
     */
    public String getName()
    {
        return name;
    }

    /**
     * Sets the name of this panel.
     * 
     * @param name The name to set.
     */
    public void setName(String name)
    {
        this.name = name;
    }

    // abstract methods follow
    
    /**
     * Initializes this panel. This method has to be implemented in
     * subclasses to construct the UI and startup the controlling logic.

     * @param parent Parent WizardFrame.
     * @param wizardData Data that is to be modified by this panel.
     * @param parameters Initialization parameters of this panel, derived from the configuration file.
     */    
    protected abstract void initialize(WizardFrame parent, WizardData wizardData, Map parameters);

    /**
     * This method is called when a property in the WizardData instance associated with
     * this wizard is changed or any other event from the UI happened.
     * 
     * @param e Event object describing the nature of the event.
     */
    public abstract void propertyChanged(PanelEvent e);
}
