/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 01.12.2005
 */

package de.tarent.doctor.core.datasources;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Vector;

import de.tarent.commons.datahandling.XMLSerializer;
import de.tarent.commons.utils.Log;

/**
 * This class implements a CSV style integration of text data that can be
 * queried using SQL commands. It uses Hypersonic as database engine to
 * access the CSV files.
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public class DoctorDatabase
{
    private static final String BEANS_TABLENAME = "DOCTOR_SERIALIZED_BEANS";
    private static final String BEANS_COLNAME_KEY = "key";
    private static final String BEANS_COLNAME_XML = "xml";
    private static final String BEANS_TABLESPEC = BEANS_COLNAME_KEY + " varchar, " + BEANS_COLNAME_XML + " varchar";
    
    private static Connection connection = null;
    private List tableList = null;
    
    /**
     * This creates a new DoctorDatabase and initializes the Hypersonic database.
     * All database files will be created or read from the directory "database" directly
     * under the root dir of the application.
     */
    public DoctorDatabase()
    {
        tableList = new Vector();
        
        if (connection==null)
        {
            Log.info(this.getClass(), "Initializing Hypersonic Database Engine.");
            try
            {
                Class.forName("org.hsqldb.jdbcDriver" );
            }
            catch (ClassNotFoundException e)
            {
                Log.error(this.getClass(), "Error initializing Hypersonic database.", e);
            }
            try
            {
                connection = DriverManager.getConnection("jdbc:hsqldb:file:database/", "sa", "");
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            Log.info(this.getClass(), "Using already available Hypersonic Database connection.");
        }
    }

    /**
     * Query the database with an arbitrary SQL request.
     * 
     * @param query SQL request.
     * @return ResultSet of SQL query results.
     */
    public ResultSet query(String query)
    {
        Statement stmt;
        try
        {
            Log.info(this.getClass(), "Query HSQL: " + query);
            stmt = connection.createStatement();
            return stmt.executeQuery(query);
        }
        catch (SQLException e)
        {
            Log.error(this.getClass(), "Error querying Hypersonic database.", e);
            return null;
        }
    }

    /**
     * Registers a CSV based table. This has to be called before any request is
     * made to a CSV based table. If the table already exists on mass storage as CSV, the
     * table is created in memory and attached to the CSV file. If the table does not
     * exist, it will be created along with the CSV file. If a table is already registered, this
     * does nothing.
     * 
     * @param name Name of the table. This will also be part of the filename of the CSV file.
     * @param colspec SQL style colspec of the table.
     */
    public void registerCSVTable(String name, String colspec)
    {
        if (!tableList.contains(name))
        {
            Log.info(this.getClass(), "Registering new table " + name + " with " + colspec + ".");
            this.query("create text table " + name + " (" + colspec + ")");
            this.query("set table " + name + " source \"" + name + ".csv;fs=,\"");
            tableList.add(name);
        }
        else
            Log.info(this.getClass(), "Table " + name + " with " + colspec + " already exists.");
    }    

    /**
     * Stores an arbitrary Serializable in the persistent database. The object will be
     * serialized to xml and stored under the given key which can be used to
     * retrieve the object later. Note that the storage is non-transient: changes to the object
     * after storage will not be reflected in the database.
     * <p>
     * If this key already exists, the entry will be overwritten by the new object.
     * 
     * @param key Key for accessing the object.
     * @param bean Serializable that should be stored.
     */
    public void storeBean(String key, Serializable bean)
    {
        // Serialize the bean to a xml string
        XMLSerializer serializer = new XMLSerializer();
        String encodedXML = serializer.serialize(bean);
        
        // Store the xml along with the key in the database
        registerCSVTable(BEANS_TABLENAME, BEANS_TABLESPEC);
        query("DELETE from " + BEANS_TABLENAME + " where " + BEANS_COLNAME_KEY + "='" + key + "'");
        query("INSERT into " + BEANS_TABLENAME + " (" + BEANS_COLNAME_KEY + ", " + BEANS_COLNAME_XML + ") values ('" + key +"', '" + encodedXML + "')");
    }
    
    /**
     * Retrieves an object instance of a Serializable from the persistent database using
     * the given key.
     * 
     * @param key Key for accessing the stored object.
     * @return Serializable representing the re-created object instance.
     */
    public Serializable retrieveBean(String key)
    {
        // Serialize the bean to a xml string
        XMLSerializer serializer = new XMLSerializer();
        
        // Retrieve the xml using the key from the database
        registerCSVTable(BEANS_TABLENAME, BEANS_TABLESPEC);
        ResultSet rs = query("SELECT xml from " + BEANS_TABLENAME + " WHERE key='" + key +"'");
        
        try
        {
            if (rs.next())
                return serializer.deserialize(rs.getString(1));
            else
                return null;
        }
        catch (SQLException e)
        {
            Log.error(this.getClass(), "Error reading bean data from HSQL.", e);
            return null;
        }
    }
}
