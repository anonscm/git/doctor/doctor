/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 12.10.2005
 */

package de.tarent.doctor.core.datasources;

import java.util.List;
import java.util.Map;

/**
 * This is the interface for implementations of DataSources used 
 * in Doctor wizards. All classes that use data from external resources
 * have to use a DataSource implementing this interface for accessing
 * the external resources. 
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public interface DoctorDataSource
{
    /**
     * This method initializes the DataSource. It claims all
     * needed resources. This is the second call to a
     * DataSource during its lifecycle.
     * 
     * @return True if initialize has succeeded, false otherwise.
     */
    public boolean initialize();

    /**
     * Disposes all resources of this DataSource. This method is
     * the last call to a DataSource during its lifecycle.
     * 
     * @return True if dispose has succeeded, false otherwise.
     */
    public boolean dispose();
    
    /**
     * Gets a chunk of data represented by the given key from the external
     * data source.
     * 
     * @param key Key for addressing the data.
     * @return Data.
     */
    public Object getData(Object key);

    /**
     * Returns all data tuples at once.
     * 
     * @return All data.
     */
    public List getAllData();
    
    /**
     * Saves a chunk of data under a given key in the external
     * data source.
     * 
     * @param key Key.
     * @param value Data.
     */
    public void setData(Object key, Object value);

    /**
     * Sets the runtime parameters for this DataSource. This will be
     * called by the FlowParser when reading the configuration. This will be
     * called as first method to a DataSource.
     * 
     * @param parameters Map with key-value pairs of the runtime configuration read from the configuration file.
     */
    public void setParameters(Map parameters);
}
