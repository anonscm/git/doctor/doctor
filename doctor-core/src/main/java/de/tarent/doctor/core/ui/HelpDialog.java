/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 13.10.2005
 */

package de.tarent.doctor.core.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;

/**
 * This class displays the about dialog.
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public class HelpDialog extends JDialog
{
    /** serialVersionUID */
	private static final long serialVersionUID = -3964518236684965503L;
	private JPanel jContentPane = null;
    private JButton jButton = null;
    private String helpFileName = null;
    
    /**
     * Constructs a new help dialog. It displays the given HTML at the url.
     * 
     * @param owner Owning frame.
     * @param title Title of the help window.
     * @param modal True if the dialog should be modal, false otherwise.
     * @param pHelpFileName URL of the HTML
     */
    public HelpDialog(Frame owner, String title, boolean modal, String pHelpFileName)
    {
        super(owner, title, modal);
        helpFileName = pHelpFileName;
        initialize();
    }

    private void initialize()
    {
        this.setContentPane(getJContentPane());
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);        
    }

    private JPanel getJContentPane()
    {
        if (jContentPane == null)
        {
            jContentPane = new JPanel();
            jContentPane.setLayout(new BorderLayout());
            jContentPane.add(getJButton(), java.awt.BorderLayout.SOUTH);
            
            JEditorPane pane = new JEditorPane();
            pane.setContentType("text/html");
            pane.setEditorKit(JEditorPane.createEditorKitForContentType("text/html"));
            pane.setFocusable( false );
            pane.setEditable(false);
            try
            {
                pane.setPage(getClass().getResource(helpFileName));
            }
            catch (IOException e)
            {
                throw new RuntimeException("Can't initialize help-file: "  + helpFileName);
            }
            
            JScrollPane scrollPane = new JScrollPane(pane);

            jContentPane.setPreferredSize(new Dimension(356, 446));
            jContentPane.add(scrollPane, "Center");
        }
        return jContentPane;
    }

    private JButton getJButton()
    {
        if (jButton == null)
        {
            jButton = new JButton();
            jButton.setText(Messages.getString("Buttons.Close"));
            jButton.addActionListener(new java.awt.event.ActionListener()
            {
                public void actionPerformed(java.awt.event.ActionEvent e)
                {
                    HelpDialog.this.setVisible(false);
                    HelpDialog.this.dispose();
                }
            });
            jButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "escapePressed");
            jButton.getActionMap().put("escapePressed", new AbstractAction() {
                public void actionPerformed(ActionEvent e) {
                	jButton.doClick(100);
                }
            });
        }
        return jButton;
    }

}  //  @jve:decl-index=0:visual-constraint="480,42"
