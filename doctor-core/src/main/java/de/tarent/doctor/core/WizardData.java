/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 06.10.2005
 */

package de.tarent.doctor.core;

import java.io.File;
import java.util.HashMap;

import de.tarent.commons.datahandling.XMLSerializer;

/**
 * This class stores all data gathered thru a wizard run. It basically 
 * stores all functional information in a key-value scheme and can be 
 * used to hold other (meta) information of the wizard. Therefore this
 * class extends the HashMap with methods for storing this additional
 * information.
 *
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public class WizardData extends HashMap
{
    /** serialVersionUID */
	private static final long serialVersionUID = 7793018306606699678L;

	/**
     * Checks whether a given key-value pair is contained in this
     * properties.
     * 
     * @param key Key
     * @param value Value
     * @return True if the properties contain the given key-value pair, false otherwise.
     */
    public boolean checkProperty(String key, Object value)
    {
        if (this.get(key)!=null && this.get(key).equals(value))
            return true;
        else
            return false;
    }

    /**
     * Saves the state of this object to a human-editable file, namely an XML.
     * 
     * @param selectedFile XML file with key-value pairs.
     */
    public void saveToFile(File selectedFile)
    {
        XMLSerializer serializer = new XMLSerializer();
        serializer.serialize(selectedFile, this);
    }

    /**
     * Reads the state of this object from a human readable file, namely an XML.
     * 
     * @param selectedFile XML file with key-value pairs.
     */
    public void loadFromFile(File selectedFile)
    {
        XMLSerializer serializer = new XMLSerializer();
        WizardData readData = (WizardData)serializer.deserialize(selectedFile);
        this.putAll(readData);
    }
    
    /**
     * Reads the state of this object from a human readable file, namely an XML.
     * 
     * @param filename XML file with key-value pairs.
     */
    public void loadFromFile(String filename)
    {
        loadFromFile(new File(filename));
    }

    /**
     * Saves the state of this object to a human-editable file, namely an XML.
     * 
     * @param filename XML file with key-value pairs.
     */
    public void saveToFile(String filename)
    {
        saveToFile(new File(filename));
    }
}
