/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 31.10.2005
 */

package de.tarent.doctor.core.engines;

import groovy.lang.GroovyClassLoader;

import java.io.File;
import java.io.IOException;

import org.codehaus.groovy.control.CompilationFailedException;

public class DoctorGroovyEngine extends DoctorEngine
{
    public void process() throws InterruptedException
    {
        String groovyScriptFileName = (String)getParameters().get("script");
     
        ClassLoader parent = getClass().getClassLoader();
        GroovyClassLoader loader = new GroovyClassLoader(parent);
        Class groovyClass = null;
        try
        {
            groovyClass = loader.parseClass(new File(groovyScriptFileName));
        }
        catch (CompilationFailedException e)
        {
            throw new RuntimeException(e);
        }
        catch (IOException e)
        {
            throw new RuntimeException("Error getting Groovy parser.");
        }

        GroovyEngine groovyObject = null;
        try
        {
            Object temp = groovyClass.newInstance();
            groovyObject = (GroovyEngine)temp;
        }
        catch (InstantiationException e)
        {
            throw new RuntimeException("Error getting Groovy object.");
        }
        catch (IllegalAccessException e)
        {
            throw new RuntimeException("Illegal access while getting Groovy object.");
        }
        
        groovyObject.run(this);        
    }
}
